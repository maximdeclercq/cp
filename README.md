# Competitive programming

This repository contains my solutions to various programming contests and
problems.
See the files whose names start with COPYING for copying permission.

Copyright years on my source files may be listed using range notation,
e.g., 2020-2025, indicating that every year in the range, inclusive, is a
copyrightable year that could otherwise be listed individually.
