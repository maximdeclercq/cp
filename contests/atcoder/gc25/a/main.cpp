#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define int int32_t

int sum_digits(int n) {
    int res = 0;
    while (n > 0) {
        res += n % 10;
        n /= 10;
    }
    return res;
}


int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	int n; cin >> n;
	int mini = INT_MAX;
	for (int a = 1; a <= n / 2; a++) {
	    mini = min(mini, sum_digits(a) + sum_digits(n - a));
	}
	cout << mini << "\n";
	
	
    return 0;
}
