#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define int __uint128_t

uint64_t N, A, B, K;
int c[300007];

int comb(int k) {
    k = min(k, N - k);
    int result = 1;
    for (int i = 1; i <= k; i++) {
        result *= (N - i + 1);
        result /= i;
    }
    result %= 998244353;
    return result;
}

int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	cin >> N >> A >> B >> K;
	fill(c, c + N + 7, 0);
	uint64_t result = 0;
	for (int r = 0; r <= N; r++) {
	    int bB = K - r * A;
	    if (bB % B != 0) continue;
	    int b = bB / B;
	    if (b < 0 || N < b) continue;
        result += (comb(r) * comb(b)) % 998244353;
        result %= 998244353;
	}
	cout << result % 998244353 << "\n";	
	
    return 0;
}
