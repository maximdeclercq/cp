#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define int int32_t

int n, d1, d2;
int v[1200][1200];

int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	cin >> n >> d1 >> d2;
	memset(v, 0, sizeof(v));
	int cnt = 0;
	for (int y = 0; y < 2 * n; y++) {
	    if (cnt == n * n) break;
	    for (int x = 0; x < 2 * n; x++) {
	        if (cnt == n * n) break;
	        if (v[x][y] != 0) continue;
	        v[x][y] = 1;
	        cout << x << " " << y << "\n";
	        cnt++;
	        if (cnt == n * n) break;
	        for (int i = x + 1; i <= max(d1 * d1, d2 * d2); i++) {
                int j1 = d1 - i * i;
                int j2 = d2 - i * i;
                int sqj1 = sqrt(j1);
                int sqj2 = sqrt(j2);
                if (sqj1 * sqj1 == j1)
                    v[i][j1] = -1;
                if (sqj2 * sqj2 == j2)
                    v[i][j2] = -1;
            }
	    }
	}
	return 0;
}
