#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define int int32_t

int n; 
int p[300000];
int calc_left(int a) {
    return p[a - 1];
}

int calc_right(int a) {
    return (n - 1) - a - (p[n - 1] - p[a]);
}

int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	cin >> n;
	fill(p, p + n, 0); 
	for (int i = 0; i < n; i++) {
	    char d; cin >> d;
	    p[i] = (i == 0 ? 0 : p[i - 1]) + (d == 'W' ? 1 : 0);
	}
	
	int res = INT_MAX;
	for (int i = 0; i < n; i++)
	    res = min(res, calc_left(i) + calc_right(i));
	
	cout << res << "\n";
	
    return 0;
}
