#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define int int32_t

int n;
int a[200000];
int solve() {
    int res = 0;
    int j;
    for (int i = 0; i < n; i = j) {
        j = i + 1;
        for (; j < n; j++) {
            if ((a[i] & a[j]) == 0) a[i] |= a[j];
            else break;
        }
        int n = (j - i);
        res += (n) * (n + 1) / 2;
    }
    return res;
}

int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	cin >> n;
	for (int i = 0; i < n; i++)
	    cin >> a[i];
    
    cout << solve() << "\n";
	
    return 0;
}
