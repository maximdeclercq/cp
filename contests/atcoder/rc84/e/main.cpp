#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define int int32_t

int data[2000];

int treemin[4005];

void buildmin(int p, int i, int j) {
    if (i == j) {
        treemin[p] = data[i];
    }
    else {
        int m = (i + j) / 2;
        buildmin(2 * p, i, m);
        buildmin(2 * p + 1, m + 1, j);
        treemin[p] = min(treemin[2 * p], treemin[2 * p + 1]);
    }
}

int querymin(int p, int i, int j, int a, int b) {
    if (j < a || i > b) {
        return 0;
    }
    else if (i >= a && j <= b) {
        return treemin[p];
    }
    else {
        int m = (i + j) / 2;
        return min(
            querymin(2 * p, i, m, a, b),
            querymin(2 * p + 1, m + 1, j, a, b)
        );
    }
}

void updatemin(int p, int i, int j, int a, int b, int newVal) {
    if (j < a || i > b) {
        return;
    }
    else if (i == j) {
        treemin[p] = newVal;
    }
    else {
        int m = (i + j) / 2;
        updatemin(2 * p, i, m, a, b, newVal);
        updatemin(2 * p + 1, m + 1, j, a, b, newVal);

        treemin[p] = min(treemin[2 * p], treemin[2 * p + 1]);
    }
}

int treemax[4005];

void buildmax(int p, int i, int j) {
    if (i == j) {
        treemax[p] = data[i];
    }
    else {
        int m = (i + j) / 2;
        buildmax(2 * p, i, m);
        buildmax(2 * p + 1, m + 1, j);
        treemax[p] = max(treemax[2 * p], treemax[2 * p + 1]);
    }
}

int querymax(int p, int i, int j, int a, int b) {
    if (j < a || i > b) {
        return 0;
    }
    else if (i >= a && j <= b) {
        return treemax[p];
    }
    else {
        int m = (i + j) / 2;
        return max(
            querymax(2 * p, i, m, a, b),
            querymax(2 * p + 1, m + 1, j, a, b)
        );
    }
}

void updatemax(int p, int i, int j, int a, int b, int newVal) {
    if (j < a || i > b) {
        return;
    }
    else if (i == j) {
        treemax[p] = newVal;
    }
    else {
        int m = (i + j) / 2;
        updatemax(2 * p, i, m, a, b, newVal);
        updatemax(2 * p + 1, m + 1, j, a, b, newVal);

        treemax[p] = max(treemax[2 * p], treemax[2 * p + 1]);
    }
}


int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	int n, k, q; cin >> n >> k >> q;
	for (int i = 0; i < n; i++) cin >> data[i];
	buildmin(0, 0, n - 1);
	buildmax(0, 0, n - 1);
	
	
	int y = 1e9 + 7;
	for (int i = 0; i < n - k; i++) {
	    
	} 
	
    return 0;
}
