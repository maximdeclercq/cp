#include <bits/stdc++.h>
using namespace std;

#define PB push_back

int n, k, cuts = 0;

int h[100000];
vector<int> graph[100000];
bitset<N> visited;

int dfs(int a) {
	visited[a] = true;
	vector<int> v;
	for (int b : graph[a]) {
		if (!visited[b])
			v.PB(dfs(b));
	}
	sort(v.begin(), v.end());
	
	int sum = h[a];
	for (int w : v) {
		if (sum + w <= k)
			sum += w;
		else
			cuts++;
	}
	return sum;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    cin >> n >> k;
    for (int i = 0; i < n; i++)
    	cin >> h[i];
  
    for (int i = 0; i < n - 1; i++) {
    	int x, y; cin >> x >> y; x--; y--;
    	graph[x].PB(y);
    	graph[y].PB(x);
    }
    
    dfs(0);
    cout << cuts << "\n";
    
    return 0;   
}
