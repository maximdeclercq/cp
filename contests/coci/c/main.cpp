#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair
#define F first
#define S second

#define PI 3.14159265
#define N 100000

#define LSO(i) ((i) & -(i))

struct v {
	double x, y;
	
	v(int x = 0, int y = 0) {
		this->x = x; this->y = y;
	}
	
	double length() {
		return sqrt(x * x + y * y);
	}
	
	double slope() {
		return y / x;
	}
	
	double angle() {
		double offset = x < 0.0 ? PI : 0;
		return atan(y / x) + offset;
	}
	
	v operator-(const v & a) {
    	return v(x - a.x, y - a.y);
	}
} p[N];

int n, q;
double ft[N];
double an[N];

double corr_angle(double angle) {
	double new_angle = asin(sin(angle));
    if (cos(angle) < 0)
    	return PI - new_angle; 
    else if (new_angle < 0)
    	return new_angle + 2 * PI;
    return new_angle;
}

double query(int i) {
	double sum = 0;
	for (i++; i > 0; i -= LSO(i))
    	sum += ft[i];
	return sum;   
}

double query(int a, int b) {
	if (b < a)
		return query(0, b) - query(a, n - 1);
	else 
		return query(b) - query(a - 1);
}

void update(int i, double v) {
   for (i++; i <= n; i += LSO(i))
      ft[i] += v;
}

double solve() {
	double m = 0;
	int p1 = 0;
	int p2 = 1;
	do {
		for (; corr_angle(an[p1] + PI) > an[p2 % n]; p2++);
		cout << p1 << " " << p2 << "\n";
		m = max(m, query(p1, p2 - 1));
		p1++;
		p1 %= n;
	} while (p1 != 0);
	return m;
}

int main() {
    ios_base::sync_with_stdio(false);
    //cin.tie(0);
	
	cin >> n;
	for (int i = 0; i < n; i++) {
		int x, y; cin >> x >> y;
		p[i] = v(x, y);
		update(i, p[i].length());
	}
	
    for (int i = n - 1; i >= 0; i--) {
		update(i, (p[(i + 1) % n] - p[i]).length());
		an[i] = ((p[(i + 1) % n] - p[i]).angle());
		an[i] = corr_angle(an[i]);
		cout << an[i] << "\n";
	}
	cout.flush();
	
	cout << solve() << "\n";
	
    cin >> q;
    for (int i = 0; i < q; i++) {
    	
    }
    
    return 0;   
}
