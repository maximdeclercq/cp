#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair
#define F first
#define S second

#define N 100

int n; 
int s[N], l[N];
bitset<N> cant_swap_s[N];
bitset<N> cant_swap_l[N];

int main() {
    ios_base::sync_with_stdio(false);
   	
    cin >> n;
    for (int i = 0; i < n; i++) {
    	cin >> s[i];
    	l[i] = s[i];
    }
    for (int lm = n - 1; lm--;) {
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if (s[i] > s[j]) { // && !(cant_swap_s[i][j] && cant_swap_s[j][i])) {
					swap(s[i], s[j]);
					cout << "query";
					for (int k = 0; k < n; k++)
						cout << " " << s[k];
					cout << endl;
					int r; cin >> r;
					if (r == 0)
						swap(s[i], s[j]);//, cant_swap_s[i][j] = cant_swap_s[j][i] = true;
					//else
					//	cant_swap_s[i].reset(), cant_swap_s[j].reset();
				}
			}
		}
    }
    for (int lm = n - 1; lm--;) {
		for (int i = 0; i < n; i++) {
			for (int j = n - 1; j >= i; j--) {
				if (l[i] < l[j]) {// && !(cant_swap_l[i][j] && cant_swap_l[j][i])) {
					swap(l[i], l[j]);
					cout << "query";
					for (int k = 0; k < n; k++)
						cout << " " << l[k];
					cout << endl;
					int r; cin >> r;
					if (r == 0)
						swap(l[i], l[j]);//, cant_swap_l[i][j] = cant_swap_l[j][i] = true;
					//else
					//	cant_swap_l[i].reset(), cant_swap_l[j].reset();
				}
			}
		}
    }
    cout << "end" << endl;
    
    for (int i = 0; i < n; i++) {
    	cout << s[i] << (i == n - 1 ? "\n" : " ");
    }
    for (int i = 0; i < n; i++) {
    	cout << l[i] << (i == n - 1 ? "\n" : " ");
    }
    
    return 0;   
}
