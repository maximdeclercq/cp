#include <bits/stdc++.h>
using namespace std;

int n, p, q;
vector<int> a;
map<int, int> memo;

int getCount(int i) {
    if (i == n - 1) return 1;
    if (i >= n) return 0;
    if (memo[i] > 0) return memo[i];
    int count = 1;
    int e = a[i] * p + q;
    for (int j = i + 1; j < n; j++) {
        if (a[j] >= e) {
            count = max(getCount(j) + 1, count);
        }
    }
    return memo[i] = count;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    int t; cin >> t;

    for (int i = 0; i < t; i++) {
        cin >> n >> p >> q;

        a.clear();
        memo.clear();
        a.resize(n + 1);
        for (int j = 0; j < n; j++) cin >> a[j];
        sort(a.begin(), a.end());

        cout << getCount(a[0]) + 1 << "\n";
    }

    return 0;
}