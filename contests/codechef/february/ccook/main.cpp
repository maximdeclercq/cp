#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    int n; cin >> n;

    for (int i = 0; i < n; i++) {
        int count = 0;
        for (int j = 0; j < 5; j++) {
            int a; cin >> a;
            count += a;
        }
        if (count == 0) {
            cout << "Beginner" << "\n";
        }
        else if (count == 1) {
            cout << "Junior Developer" << "\n";
        }
        else if (count == 2) {
            cout << "Middle Developer" << "\n";
        }
        else if (count == 3) {
            cout << "Senior Developer" << "\n";
        }
        else if (count == 4) {
            cout << "Hacker" << "\n";
        }
        else if (count == 5) {
            cout << "Jeff Dean" << "\n";
        }
    }

    return 0;
}