#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    int t; cin >> t;

    for (int i = 0; i < t; i++) {
        int n; cin >> n;
        int a, b, c; cin >> a >> b >> c;

        vector<int> div;
        for (int j = 1; j <= sqrt(n); j++) {
            if (n % j == 0) {
                int p = n / j;
                if (j != p) {
                    div.push_back(p);
                }
                div.push_back(j);
            }
        }
        sort(div.begin(), div.end());

        int numDiv = div.size();

        int v = 0;
        for (int x = 0; x < numDiv && div[x] <= a; x++) {
            for (int y = 0; y < numDiv && div[y] <= b; y++) {
                int p = div[x] * div[y];
                if (p > n) break;
                for (int z = 0; z < numDiv && div[z] <= c; z++) {
                    int q = p * div[z];
                    if (q > n) break;
                    if (n == div[x] * div[y] * div[z]) v++;
                }
            }
        }
        cout << v << "\n";
    }

    return 0;
}