#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define int int64_t

int n, m;
int grid[1000][1000];
int hsum[1000][1000];
int vsum[1000][1000];
int sum[1000][1000];

void generate_sums() {
    for (int y = 0; y < n; y++) {
        for (int x = 0; x < m; x++) {
            hsum[x][y] = grid[x][y] + (x == 0 ? 0 : hsum[x - 1][y]);
        }
    }
    for (int x = 0; x < m; x++) {
        for (int y = 0; y < n; y++) {
            vsum[x][y] = grid[x][y] + (y == 0 ? 0 : vsum[x][y - 1]);
        }
    }
}

int calc(int x, int y) {
    int mx1 = -1e10, curr = 0;
    for (int i = x - 1; i >= 0; i--)
    {
        curr = curr + grid[i][y];
        mx1 = max(mx1, curr);
        curr = curr < 0 ? 0 : curr;
    }
    int mx2 = -1e10; curr = 0;
    for (int i = x + 1; i < m; i++) {
        curr = curr + grid[i][y];
        mx2 = max(mx2, curr);
        curr = curr < 0 ? 0 : curr;
    }
    int my1 = -1e10; curr = 0;
    for (int i = y - 1; i >= 0; i--) {
        curr = curr + grid[x][i];
        my1 = max(my1, curr);
        curr = curr < 0 ? 0 : curr;
    }
    int my2 = -1e10; curr = 0;
    for (int i = y + 1; i < n; i++) {
        curr = curr + grid[x][i];
        my2 = max(my2, curr);
        curr = curr < 0 ? 0 : curr;
    }
    return mx1 + mx2 + my1 + my2 + grid[x][y];
}

int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	int t; cin >> t;
	while (t--) {
	    cin >> n >> m;
	    for (int y = 0; y < n; y++) {
	        for (int x = 0; x < m; x++) {
	            cin >> grid[x][y];    
	        }
	    }
	    
	    generate_sums();
	    
	    int ma = -1e10;
	    for (int y = 1; y < n - 1; y++) {
	        for (int x = 1; x < m - 1; x++) {
	            ma = max(ma, calc(x, y));
	        }
	    }
	    cout << ma << "\n";
	}
	
    return 0;
}
