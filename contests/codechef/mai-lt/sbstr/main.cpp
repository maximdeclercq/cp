#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define int int32_t

int k;
string s;

int solve() {
    int res = 0;
    for (int i = 0; i < (int)s.size(); i++) {
        int distinct = 0;
        int count[256];
        fill(count, count + 256, 0);
        int m = 0;
        for (int j = i; j < (int)s.size(); j++) {
            m = max(m, ++count[(int)s[j]]);
            if (count[(int)s[j]] == 1) distinct++;
            if (((j - i + 1) % distinct) != 0) continue;
            if (((j - i + 1) / distinct) != m) continue;
            if (distinct >= k) res++;
        }
    }
    return res;
}

int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	int t; cin >> t;
	while (t--) {
	    cin >> s >> k;
	    cout << solve() << "\n";
	}
	
    return 0;
}
