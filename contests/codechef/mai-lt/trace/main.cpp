#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define int int32_t

int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	int t; cin >> t;
	while (t--) {
	    int n; cin >> n;
	    
	    int trace[2 * n - 1];
	    fill(trace, trace + 2 * n - 1, 0);
	    int m = 0;
	    for (int i = 0; i < n; i++) {
	        for (int j = 0; j < n; j++) {
	            int a; cin >> a;
	            m = max(m, trace[i - j + n - 1] += a);
	        }
	    }
	    cout << m << "\n";
	}
	
    return 0;
}
