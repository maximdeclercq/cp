#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define ii pair<int, int>
#define iii pair<int, pair<int, int>>

#define int int32_t

int t, n, m;
bool grid[300][300];
bitset<300> vk[300];
bitset<300> vc[300];

int num = 0;

void solve1(int xk, int yk) {
    if (xk >= m) return;
    if (yk < 0 || yk >= n) return;
    if (vk[xk][yk]) return;
    
    bool kreached = xk >= m - 1 && yk <= 0;
    
    if (kreached) { 
        num++;
        return;
    }
    
    vk[xk][yk] = true;
    
    solve(xk, yk - 1);
    solve(xk, yk + 1);
    solve(xk + 1, yk);
    
    vk[xk][yk] = false;
}

void solve2(int xk, int yk) {
    if (xk >= m) return;
    if (yk < 0 || yk >= n) return;
    if (vk[xk][yk]) return;
    
    bool kreached = xk >= m - 1 && yk >= n - 1;
    
    if (kreached) { 
        num++;
        return;
    }
    
    vk[xk][yk] = true;
    
    solve(xk, yk - 1);
    solve(xk, yk + 1);
    solve(xk + 1, yk);
    
    vk[xk][yk] = false;
}

int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	cin >> t;
	while (t--) {
	    cin >> n >> m;
	    
	    for (int i = 0; i < m; i++) {
	        vk[m].reset();
	    }
	    num = 0;
	    
	    for (int y = 0; y < n; y++) {
	        for (int x = 0; x < m; x++) {
	            char c; cin >> c;
	            vk[x][y] = c == '#';
	        }
	    }
	    
	    solve1(0, 0);
	    solve2(0, 0);
	    cout << num / 2 << "\n";
	}
	
    return 0;
}
