#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define ii pair<int, int>
#define iii pair<int, pair<int, int>>

int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	int t; cin >> t;
	while (t--) {
	    double xc, xk, xb; cin >> xc >> xk >> xb;
	    double vc, vk; cin >> vc >> vk;
	    
	    double tc = (xb - xc) / vc;
	    double tk = (xk - xb) / vk;
	    if (tc < tk) {
	        cout << "Chef\n";
	    }
	    else if (tc > tk) {
	        cout << "Kefa\n";
	    }
	    else {
	        cout << "Draw\n";
	    }
	}
	
    return 0;
}
