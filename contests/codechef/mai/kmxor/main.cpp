#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define ii pair<int, int>
#define iii pair<int, pair<int, int>>

#define int int64_t

int32_t main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
	
	int t; cin >> t;
	while (t--) {
	    int n, k;
	    cin >> n >> k; n--;
	    
	    int r = k;
	    
	    vector<int> res(n);
        int order = 0;
        for (int i = 0; i < 32; i++) {
            if ((k & (1 << i)) != 0)
                order = i;
        }
        for (int i = 0; i < order; i++) {
            if ((k & (1 << i)) == 0)
                res[0] |= (1 << i);
        }
        for (int i = 0; i < n; i++) {
            if (res[i] != 0) continue;
            bool succes = false;
            int ord = 0;
            while (!succes) {
                for (int j = 0; j < n; j++) {
                    if (i == j) continue;
                    if (res[j] == 0 || (((res[j] & (1 << ord)) == 0) && ((res[j] | (1 << ord)) <= k))) {
                        succes = true;
                        res[i] |= (1 << ord);
                        res[j] |= (1 << ord);
                        break;
                    }
                }
                ord++;
                if (ord > order) {
                    r ^= 1;
                    res[i] ^= 1;
                    break;
                }
            }
        }
	    
	    cout << r;
	    for (int i = 0; i < n; i++) {
	        cout << " " << res[i];
	    }
	    cout << "\n";
	}
	
    return 0;
}
