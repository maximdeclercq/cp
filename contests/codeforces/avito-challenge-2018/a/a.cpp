#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define int int32_t

int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	string s; cin >> s;
	int l = s.size();
	int r = 0;
	for (int i = 0; i < l; i++) {
	    for (int j = i; j < l; j++) {
	        bool palin = true;
	        for (int k = i; k <= j / 2; k++) {
	            palin &= s[k] == s[j - k];
	            if (!palin) {
	                r = max(j - i + 1, r);
	                break;
	            }
	        }
	    }
	}
	cout << r << "\n";
	
    return 0;
}
