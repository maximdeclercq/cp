#include <bits/stdc++.h>
using namespace std;

#define V vector
#define M map
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define int uint64_t

M<int, int> v;
int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	int inc = 0;
	
	int n; cin >> n;
	for (int i = 0; i < n; i++) {
	    int a, x; cin >> a >> x;
	    inc += x;
	    v[a] = x;
	}
	
	int m; cin >> m;
	for (int i = n; i < n + m; i++) {
	    int b, y; cin >> b >> y;
	    inc += y;
	    if (v[b] != 0) {
	        inc -= min(y, v[b]);
	    }
	}
	
	cout << inc << "\n";
	
    return 0;
}
