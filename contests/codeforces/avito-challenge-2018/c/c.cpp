#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define int int32_t

int degree[200007];
V<int> graph[200007];
bitset<200007> vis;
int result[200007];
int cnt = 0;
int num = 0;

void dfs(int i, int root) {
    if (!vis[i]) cnt++;
    vis[i] = true;
    bool succes = false;
    for (int j : graph[i]) {
        if (vis[j]) continue;
        dfs(j, root);
        succes = true;
        if (i != root) break;
    }
    if (!succes)
        result[num++] = i;
}

int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	int n; cin >> n;
	for (int i = 0; i < n - 1; i++) {
	    int a, b; cin >> a >> b;
	    graph[a].PB(b);
	    graph[b].PB(a);
	    degree[a]++;
	    degree[b]++;
	}
	
	P<int, int> best[n];
	for (int i = 1; i <= n; i++) best[i] = MP(-degree[i], i);
	sort(best, best + n);
	
	int i = 0;
	for (; i < n; i++) {
	    dfs(best[i].S, best[i].S);
	    if (cnt == n) break;
	    vis.reset();
	    cnt = 0;
	    num = 0;
	}
	
	if (num == 0) {
	    cout << "No\n";
	}
	else if (num != 0) {
	    cout << "Yes\n" << num << "\n";
	    for (int j = 0; j < num; j++) {
	        cout << best[i].S << " " << result[j] << "\n";
	    }
	}
	
    return 0;
}
