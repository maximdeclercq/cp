#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define int int64_t

int n, k;
int a[50];
int p[50];
int memo[50][50];

int solve(int s = 0, int i = 0, int an = LLONG_MAX) {
    if (an == 0) return 0;
    int maxi = 0;
    int sum = 0;
    for (int j = i; j < n; j++) {
        sum += a[j];
        int r = s == k - 1 ? an & sum : solve(s + 1, j + 1, an & sum); 
        maxi = max(maxi, r);
    }
    return maxi;
}

int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	cin >> n >> k;
	for (int i = 0; i < n; i++) cin >> a[i];
    for (int i = 0; i < n; i++) p[i] = a[i] + (i == 0 ? 0 : p[i - 1]);
    
	cout << solve() << "\n";
	
    return 0;
}
