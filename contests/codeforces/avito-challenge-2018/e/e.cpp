#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define X(i) p[i].S
#define L(i) p[i].F.F
#define R(i) p[i].F.S

#define int int64_t

int n, q;
P<P<int, int>, int> p[10007];
bitset<10007> possible;

void calc(int i = 0, int w = 0, int lb = 0, int rb = 10007) {
    if (lb > rb) return;
    if (w > n) return;
    possible[w] = true;
    
    if (i == q) return;
    calc(i + 1, w, lb, rb);
    if (possible[w + X(i)]) return;
    calc(i + 1, w + X(i), L(i), min(rb, R(i)));
}

int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	cin >> n >> q;
	for (int i = 0; i < q; i++) {
	    int l, r, x; cin >> l >> r >> x;
	    p[i] = MP(MP(l, r), x);
	}
	sort(p, p + q);
	
	calc();
	
	V<int> maxs;
	for (int i = 1; i <= n; i++) {
	    if (possible[i]) maxs.PB(i);
	}
	
	cout << maxs.size() << "\n";
    for (int i = 0; i < maxs.size(); i++) {
        cout << maxs[i] << (i == maxs.size() - 1 ? "\n" : " ");
    }
	
    return 0;
}
