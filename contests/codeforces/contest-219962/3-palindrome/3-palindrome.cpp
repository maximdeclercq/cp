#include <bits/stdc++.h>
using namespace std;

bool isMultipleOf(int a, int b = 3) {
    return a % b == 0;
}

int main() {
    ios_base::sync_with_stdio(false);

    int n;
    cin >> n;

    string out = "";
    out.assign(n, 'a');

    for (int i = 0; i < n; i+=4) {
        out[i] = 'b';
        if (i + 1 < n) {
            out[i + 1] = 'b';
        }
    }

    cout << out << "\n";
}