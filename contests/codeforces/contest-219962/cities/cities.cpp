#include <bits/stdc++.h>
using namespace std;

int main() {
    ios_base::sync_with_stdio(false);

    int n;
    cin >> n;

    cout << (n - 1) / 2 << "\n"; 

    return 0;   
}