#include <bits/stdc++.h>
using namespace std;

bool isMultiple(string s) {
    return atoi(s.data()) % 3 == 0;
}

int main() {
    ios_base::sync_with_stdio(false);

    string s;
    cin >> s;

    int number = atoi(s.data());
    int diff = number % 3;

    if (diff == 0) {
        cout << number << "\n";
        return 0;
    }
    else {
        vector<pair<int, int>> numbers;

        for (int i = 0; i < (int)s.size(); i++) {
            int a = int(s[i]) - 48;
            if (a % 3 > 0) {
                numbers.push_back(make_pair(a % 3, i));
            }
        }

        if (diff == 1) {
            auto it1 = find(numbers.begin(), numbers.end(), make_pair(1, 0));
            if (it1 == numbers.end()) {
                auto it2a = find(numbers.begin(), numbers.end(), make_pair(2, 0));
                it2a->first = 0;
                auto it2b = find(numbers.begin(), numbers.end(), make_pair(2, 0));
                if (it2a == numbers.end() || it2b == numbers.end()) {
                    cout << -1 << "\n";
                    return 0;
                }
                else {
                    s.erase(it2a->second, 1);
                    s.erase(it2b->second, 1);
                }
            }
            else {
                s.erase(it1->second, 1);
            }
        }
        else if (diff == 2) {
            auto it2 = find(numbers.begin(), numbers.end(), make_pair(2, 0));
            if (it2 == numbers.end()) {
                auto it1a = find(numbers.begin(), numbers.end(), make_pair(1, 0));
                it1a->first = 0;
                auto it1b = find(numbers.begin(), numbers.end(), make_pair(1, 0));
                if (it1a == numbers.end() || it1b == numbers.end()) {
                    cout << -1 << "\n";
                    return 0;
                }
                else {
                    s.erase(it1a->second, 1);
                    s.erase(it1b->second, 1);
                }
            }
            else {
                cout <<"R";
                s.erase(it2->second, 1);
            }
        }
    }

    while (s[0]=='0') { s.erase(0, 1); }

    cout << s << "\n";

    return 0;
    
}