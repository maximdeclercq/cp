#include <bits/stdc++.h>
using namespace std;

vector<int> stations;
vector<pair<int, int>> roads;

int main() {
    ios_base::sync_with_stdio(false);

    int n, k, d;
    cin >> n >> k >> d;

    stations.assign(n, 0);

    for (int i = 0; i < k; i++) {
        int c;
        cin >> c;
        stations[c - 1]++;
    }

    for (int i = 0; i < n - 1; i++) {
        int u, v;
        cin >> u >> v;
        roads.push_back(make_pair(u, v));
    }    
}