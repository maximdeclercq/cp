#include <bits/stdc++.h>
using namespace std;

int main() {
    ios_base::sync_with_stdio(false);

    int u, p, up;
    cin >> u >> p >> up;

    int m;
    cin >> m;

    vector<pair<int, bool>> mice; // true = usb

    for (int i = 0; i < m; i++) {
        int v;
        string t;
        cin >> v >> t;
        mice.push_back(make_pair(v, t == "USB"));
    }

    sort(mice.begin(), mice.end());

    int comp = 0;
    int price = 0;

    for (int i = 0; i < m; i++) {
        pair<int, bool> v = mice[i];
        if (v.second) { //USB
            if (u > 0) {
                u--;
                comp++;
                price += v.first;
            }
            else if (up > 0) {
                up--;
                comp++;
                price += v.first;
            }
        }
        else {
            if (p > 0) {
                p--;
                comp++;
                price += v.first;
            }
            else if (up > 0) {
                up--;
                comp++;
                price += v.first;
            }
        }
    }

    cout << comp << " " << price << "\n";
}