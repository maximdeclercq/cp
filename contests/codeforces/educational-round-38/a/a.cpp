#include <bits/stdc++.h>
using namespace std;

bool isVowel(char c) {
    return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'y';
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    int n; cin >> n;
    string s; cin >> s;

    for (int i = 1; i < n; i++) {
        if (isVowel(s[i - 1]) && isVowel(s[i])) {
            s.erase(i, 1);
            i--;
            n--;
        }
    }

    cout << s << "\n";
}