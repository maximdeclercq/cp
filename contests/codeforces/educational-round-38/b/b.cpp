#include <bits/stdc++.h>
using namespace std;

bool isVowel(char c) {
    return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'y';
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    int n; cin >> n;


    int seconds = 0;
    for (int i = 0; i < n; i++) {
        int a; cin >> a;

        if (a <= 500'000) {
            seconds = max(seconds, a - 1);
        }
        else {
            seconds = max(seconds, 1'000'000 - a);
        }
    }

    cout << seconds << "\n";
}