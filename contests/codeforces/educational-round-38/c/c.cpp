#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    int t; cin >> t;

    for (int i = 0; i < t; i++) {
        int x; cin >> x;

        if (x == 0) {
            cout << "1 1\n";
        }
        else {
            int n = sqrt(x * 2.0 * 2.0 / 3.0);
            if (n >= 2) {
                cout << n << " 2\n";
            }
            else {
                cout << "-1\n";
            }
        }
    }
}