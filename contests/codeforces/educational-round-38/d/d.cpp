#include <bits/stdc++.h>
using namespace std;

vector<vector<pair<int, int>>> neigh;

vector<int> ticketcost;
vector<int> totalcost;

pair<int, int> dijkstra(int i, int n) {
    totalcost.clear();
    totalcost.assign(n, INT_MAX);

    priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> q;
    q.push(make_pair(0, i));
    totalcost[i] = 0;

    pair<int, int> mincost = make_pair(i, ticketcost[i]);
    while(!q.empty()) {
        int c = q.top().second; q.pop();

        for (auto d : neigh[c]) {
            int a = d.first;
            int b = d.second;

            int cost = totalcost[c] + b;
            
            if (totalcost[a] > cost + ticketcost[a]) {
                mincost.first = a;
                mincost.second = totalcost[a] = cost + ticketcost[a];
                q.push(make_pair(cost, a));
            }
        }
    }
    return mincost;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    int n, m; cin >> n >> m;

    neigh.assign(n, vector<pair<int, int>>());
    ticketcost.assign(n, 0);

    for (int i = 0; i < m; i++) {
        int u, v, w; cin >> u >> v >> w;
        neigh[--u].push_back(make_pair(--v, w));
        neigh[v].push_back(make_pair(u, w));
    }

    for (int i = 0; i < n; i++) {
        int a; cin >> a;
        ticketcost[i] = a; 
    }

    vector<int> minCosts(n, -1);

    for (int i = 0; i < n; i++) {
        if (minCosts[i] == -1) {
            pair<int, int> p = dijkstra(i, n);
            minCosts[i] = p.second;
            minCosts[p.first] = ticketcost[p.first];
        }
        cout << minCosts[i] << " ";
    }
    cout << "\n";
}