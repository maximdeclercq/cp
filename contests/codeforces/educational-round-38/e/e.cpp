#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    uint32_t n; cin >> n;

    vector<uint32_t> a(n);
    vector<uint32_t> p(n);
    for (uint32_t i = 0; i < n; i++) cin >> a[i];
    for (uint32_t i = 0; i < n; i++) p[i] = i;

    uint64_t fa = 0;
    uint32_t m = 0;

    do {
        for (uint32_t i = 1; i < n; i++) {
            if (a[p[m]] < a[p[i]]) {
                fa += a[p[m]];
                fa %= 1'000'000'007;
                m = i;
            }
        }

    } while(next_permutation(p.begin(), p.end()));

    cout << fa % 1'000'000'007 << "\n";
}