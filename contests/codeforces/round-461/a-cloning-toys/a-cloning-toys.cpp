#include <bits/stdc++.h>
using namespace std;

int main() {
    unsigned long long o, c;
    cin >> c >> o;
    if (o <= 0 || c + 1 < o || (c - o) % 2 == 0 || (o == 1 && c >= o)) {
        cout << "No" << "\n";
    }
    else {
        cout << "Yes" << "\n";
    }
}