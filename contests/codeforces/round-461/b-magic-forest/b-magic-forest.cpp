#include <bits/stdc++.h>
using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int n;
    cin >> n;

    int count = 0;
    for (int i = 1; i <= n; i++) {
        for (int j = i; j <= n; j++) {
            for (int k = j; k <= min(i + j - 1, n); k++) {
                if ((i ^ j ^ k) == 0) count++;
            }
        }
    }
    cout << count << "\n";
}