#include <bits/stdc++.h>
using namespace std;

typedef unsigned long long ull;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    ull n, k;
    cin >> n >> k;

    if (n > k || k == 1) {
        cout << "Yes" << "\n";
    }
    else {
        cout << "No" << "\n";
    }
    return 0;
}