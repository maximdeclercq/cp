#include <bits/stdc++.h>
using namespace std;

typedef unsigned long long ull;

set<ull> s;
multiset<ull> ms;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    ull n, k;
    cin >> n >> k;

    for (ull i = 1; i <= n; i++) {
        for (ull j = i + 1; j <= n; j++) {
            if (j % i == 0) {
                s.insert(i);
                s.insert(j);
                ms.insert(i);
                ms.insert(j);
            }
        }
    }

    ull size = ms.size() / 2;

    ull p = 0;
    while (size > k && p < k) {
        for (auto a : s) {
            if (ms.count(size) + p == k) {
                s.erase(a);
                ms.erase(a);
            }
        }
        p++;
    }

    if (size == k) {
        cout << "Yes" << "\n";
        cout << s.size() << "\n";
        for (auto a : s) {
            cout << a << " ";
        }
        cout << "\n";
    }
    else {
        cout << "No" << "\n";
    }

    return 0;
}