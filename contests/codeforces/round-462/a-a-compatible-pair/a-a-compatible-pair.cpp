#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    int n, m;
    cin >> n >> m;

    deque<int> aQ;
    for (int i = 0; i < n; i++) {
        int a;
        cin >> a;
        aQ.push_back(a);
    }

    deque<int> bQ;
    for (int i = 0; i < m; i++) {
        int b;
        cin >> b;
        bQ.push_back(b);
    }

    sort(aQ.begin(), aQ.end());
    sort(bQ.begin(), bQ.end());

    int s = bQ.front();
    int l = bQ.back();

    if (n > 2) {
        if (aQ.front() * s > aQ.back() * s) {
            aQ.pop_front();
        }
        if (aQ.back() * l > aQ.front() * l) {
            aQ.pop_back();
        }
    }
    else {
        if (aQ.front() * s > aQ.back() * s) {
            aQ.pop_front();
        }
        else if (aQ.back() * l > aQ.front() * l) {
            aQ.pop_back();
        }
    }

    int result = max(max(aQ.front() * bQ.front(), aQ.back() * bQ.back()), max(aQ.front() * bQ.back(), aQ.back() * bQ.front()));
    cout << result << "\n";
}