#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    int64_t p;
    int64_t k;
    cin >> p >> k;

    vector<int> coeff;
    while (p != 0) {
        int64_t r = p % -k;
        p /= -k;

        if (r < 0) {
            r += k;
            p++;
        }

        coeff.push_back(r);
    }

    any a;

    cout << size(coeff) << "\n";
    for (auto c : coeff) {
        cout << c << " ";
    }
    cout << "\n";

    return 0;
}