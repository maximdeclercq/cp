#include <bits/stdc++.h>
using namespace std;

bool intersects(const tuple<int, int, int> a, const tuple<int, int, int> b) {
    int dx = get<0>(a) - get<0>(b);
    int dy = get<1>(a) - get<1>(b);
    int dSq = dx * dx + dy * dy;
    
    int dr = get<2>(a) - get<2>(b);
    int drSq = dr * dr;

    int sr = get<2>(a) + get<2>(b);
    int srSq = sr * sr;
    
    bool intersects = drSq < dSq && dSq < srSq;
    return intersects;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    int n;
    cin >> n;

    vector<tuple<int, int, int>> c(n);

    for (int i = 0; i < n; i++) {
        int x, y, r;
        cin >> x >> y >> r;
        c[i] = make_tuple(x, y, r);
    }

    int inters = n + 1;
    for (int b = 0; b < (1 << n); b++) {
        int numBits = __builtin_popcount(b);
        if (numBits > 1) {
            for (int i = 0; i < n; i++) {
                if (b & (1 << i)) {
                    int count = 0;
                    for (int j = i + 1; j < n; j++) {
                        if (b & (1 << j)) {
                            count += intersects(c[i], c[j]);
                        }
                    }
                    inters += (count == numBits - 1) ? 1 : 0;
                }
            }
        }
    }

    cout << inters << "\n";

    return 0;
}