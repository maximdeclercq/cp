#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    int n; cin >> n;
    
    vector<int> f(n);
    for (int i = 0; i < n; ++i) {
        int a; cin >> a;
        f[i] = --a;
    }

    for (int i = 0; i < n; ++i) {
        if (f[f[f[i]]] == i) {
            cout << "YES\n";
            return 0;
        }
    }
    cout << "NO\n";
    return 0;
}