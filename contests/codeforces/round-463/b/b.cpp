#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    uint64_t n; cin >> n;
    uint64_t k; cin >> k;

    uint64_t min = UINT64_MAX;
    uint64_t typeBoxes = UINT64_MAX;
    uint64_t numBoxes = UINT64_MAX;
    for (uint64_t i = 0; i < k; i++) {
        uint64_t a; cin >> a;

        uint64_t b = n % a;
        if (b < min || min == UINT64_MAX) {
            min = b;
            typeBoxes = i + 1;
            numBoxes = n / a;
        }
    }

    cout << typeBoxes << " " << numBoxes << "\n";
    return 0;
}