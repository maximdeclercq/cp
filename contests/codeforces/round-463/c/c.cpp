#include <bits/stdc++.h>
using namespace std;

#define LSO(i) ((i) & (-(i)))

int n;
vector<int> ft;

int rsq(int a) {
    int sum = 0;
    for (; a > 0; a -= LSO(a))
        sum += ft[a];
    return sum;
}

int rsq(int a, int b) {
    return rsq(b) - rsq(a - 1);
}

void update(int a, int v) {
    for (; a <= n; a += LSO(a))
        ft[a] += v;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    cin >> n;
    
    ft.assign(n, 0);

    vector<int> p(n);
    for (int i = 1; i <= n; i++) {
        int a; cin >> a;
        update(i, a);
    }

    int s; cin >> s;
    int f; cin >> f;

    int max = 0;
    int imax = 0;
    // i = deltatime in 1st time zone
    for (int i = 1; i <= n; i++) {
        int beg = s - i + 1;
        int end = f - i;

        int x;
        if (beg < 1 && end < 1) {
            beg += n;
            end += n;
            x = rsq(beg, end);
        }
        else if (beg < 1) {
            beg += n;
            x = rsq(1, end) + rsq(beg, n);
        }
        else {
            x = rsq(beg, end);
        }

        if (x > max) {
            imax = i;
            max = x;
        }
        //cout << i << " " << beg << "-" << end << " : " << x << "\n";        
    }

    cout << imax << "\n";

    return 0;
}