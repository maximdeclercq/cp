#include <bits/stdc++.h>
using namespace std;

vector<double> mean;
vector<int> data;


void build(int p, int i, int j) {
    if (i == j) {
        mean[p] = 0;
    }
    else {
        int m = (i + j) / 2;
        build(2 * p, i, m);
        build(2 * p + 1, m + 1, j);

        mean[p] = (mean[2 * p] + mean[2 * p + 1]) / 2;
    }
}

double getMean(int p, int i, int j, int a, int b) {
    if (j < a || i > b) {
        return 0;
    }
    else if (i >= a && j <= b) {
        return mean[p];
    }
    else {
        int m = (i + j) / 2;
        return (getMean(2 * p, i, m, a, b) + getMean(2 * p + 1, m + 1, j, a, b)) / 2;
    }
}

void updateMean(int p, int i, int j, int a, double newVal) {
    if (i == j) {
        mean[p] = newVal;
    }
    else {
        int m = (i + j) / 2;
        if (a <= i) {
            updateMean(2 * p, i, m, a, newVal);
        }
        else {
            updateMean(2 * p + 1, m + 1, j, a, newVal);
        }

        mean[p] = (mean[2 * p] + mean[2 * p + 1]) / 2;
    }
}


int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    int q; cin >> q;
    int index = 1;
    int maxS = 0;

    data.assign(5 * 100000, 0);
    mean.assign(5 * 1000000, 0);
    build(1, 1, 5 * 100000);

    for (int i = 0; i < q; i++) {
        int t; cin >> t;

        if (t == 1) {
            int a; cin >> a;
            updateMean(index, 1, index, index, a);
            maxS = max(a, maxS);
            data[index] = a;
            index++;
        }
        else if (t == 2) {
            double maximum = 0;
            for (int c = 1; c <= index; c++) {
                maximum = max(maxS - getMean(1, 1, index, c, index), maximum);
            }
            cout << maximum << "\n";
        }
    }
}