#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define int int32_t

map<string, int> a = {{"purple", 0}, {"green", 1}, {"blue", 2}, {"orange", 3}, {"red", 4}, {"yellow", 5}};
map<int, string> b = {{0, "Power"}, {1, "Time"}, {2, "Space"}, {3, "Soul"}, {4, "Reality"}, {5, "Mind"}};
bitset<6> h;

int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	int n; cin >> n;
	for (int i = 0; i < n; i++) {
	    string s; cin >> s;
	    h[a[s]] = true;
	}
	
	cout << 6 - n << "\n";
	for (int i = 0; i < 6; i++) {
	    if (h[i]) continue;
	    cout << b[i] << "\n";
	}
	
	
	
    return 0;
}
