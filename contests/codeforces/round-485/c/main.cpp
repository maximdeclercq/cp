#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define MAX 1e12
#define int int64_t

int n;
int s[3000];
int c[3000];
int minis[3000];

int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	cin >> n;
	for (int i = 0; i < n; i++)
	    cin >> s[i];
	for (int i = 0; i < n; i++)
	    cin >> c[i];
	
	fill(minis, minis + n, MAX);
	
	for (int i = 0; i < n; i++) {
	    for (int j = i + 1; j < n; j++) {
            if (s[i] >= s[j]) continue;
            minis[i] = min(minis[i], c[i] + c[j]);
	    }
	}
	
	int mini = MAX;
	for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            if (s[i] >= s[j]) continue;
            mini = min(mini, minis[j] + c[i]);
        }
	}
	
	if (mini == MAX) mini = -1;
	cout << mini << "\n";
	
    return 0;
}
