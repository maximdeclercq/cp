#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    
    int n; cin >> n;
    vector<vector<int>> adj(n, vector<int>());
    
    vector<pair<int, int>> s(n);
    for (int i = 0; i < n; i++) {
        int l, e; cin >> l >> e;
        s[i] = make_pair(l, e);
    }

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (i == j) continue;
            if (s[i].first == s[j].first || s[i].second == s[j].second) {
                adj[i].push_back(j);
                //adj[j].push_back(i);
            }
        }
    }

    priority_queue<pair<int, int>> pq;
    for (int i = 0; i < n; i++) {
        int s = adj[i].size();
        if (s > 0) pq.push(make_pair(s, i));
    }

    int count = 0;
    vector<pair<int, int>> result;

    bitset<1000000> t(0);
    while (pq.size() > 0) {
        pair<int, int> p = pq.top(); pq.pop();
        int s = p.first;
        int a = p.second;
        
        for (int b : adj[a]) {
            if (t.test(b)) continue;
            
            count++;
            t.set(a);
            t.set(b);
            result.push_back(make_pair(a, b));
            break;
        }
    }

    cout << count << "\n";
    for (pair<int, int> a : result)
        cout << a.first + 1 << " " << a.second + 1 << "\n";
   
    return 0;
}

