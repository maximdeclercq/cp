#include <bits/stdc++.h>
using namespace std;

vector<int> arr; 

void solve(int a, int b) {
    cout << "(";
    if (a == b) {
        cout << arr[a]; 
    }
    else {
        solve(a, (a + b) / 2);
        solve((a + b) / 2 + 1, b);
    }
    cout << ")";
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    int n; cin >> n;
    vector<int> w(n); for (int i = 0; i < n; i++) cin >> w[i];

    sort(w.begin(), w.end());
    
    for (int i = 0; i < n / 2; i++) {
        arr.push_back(w[n - i - 1]);
        arr.push_back(w[i]);
    }
    
    /*for (int i = 0; i < n; i++) {
        arr.push_back(w[i]);
    }*/
    
    solve(0, n - 1);
    cout << "\n";
    
    return 0;
}
