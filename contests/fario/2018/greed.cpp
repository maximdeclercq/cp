#include <bits/stdc++.h>
using namespace std;

int r, c, k, h, v; 
int x[1500];
int y[1500];

struct rect {
    int x1, y1, x2, y2;
    
    rect(int x1 = 0, int y1 = 0, int x2 = 0, int y2 = 0) : x1(x1), y1(y1), x2(x2), y2(y2) {}
    
    int a() {
        return (x2 - x1) * (y2 - y1);
    }
};

int solve(rect r, int l) {
    if (l == 0) return -1;
    rect sr;
    int res = 0;
    
    for (int i = 0; i < v; i++) {
        if (x[i] <= r.x1 || x[i] >= r.x2) continue;
        
        rect left = r;
        left.x2 = x[i];
        rect right = r;
        right.x1 = x[i];

        if (right.a() < sr.a() || sr.a() == 0) {
            if (right.a() * (l + 1) > r.a()) {
                int res = solve(left, l - 1);
                if (res < 0) {
                    sr = right;
                }
            }
        }
    }
    for (int i = 0; i < h; i++) {
        if (y[i] <= r.y1 || y[i] >= r.y2) continue;
        
        rect bottom = r;
        bottom.y2 = y[i];
        rect top = r;
        top.y1 = y[i];

        if (top.a() < sr.a() || sr.a() == 0) {
            if (top.a() * (l + 1) > r.a()) {
                int res = solve(bottom, l - 1);
                if (res > 0) {
                    sr = top;
                }
            }
        }
    }

    return sr.a();
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    cin >> r >> c >> k >> h >> v;
    for (int i = 0; i < h; i++) cin >> y[i];
    for (int i = 0; i < v; i++) cin >> x[i];

    cout << solve(rect(0, 0, r, c), k) << "\n";

    return 0;
}
