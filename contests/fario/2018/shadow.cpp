#include <bits/stdc++.h>
using namespace std;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    int n; cin >> n;
    vector<bitset<3000>> west(n, bitset<3000>(0));
    int wmin = INT_MAX;
    int wmax = 0;
    for (int i = 0; i < n; i++) {
        int a, b; cin >> a >> b;
        wmin = min(wmin, a);
        wmax = max(wmax, b);
        for (int j = a; j <= b; j++)
            west[i][j] = true;
    }
    
    vector<bitset<3000>> client(n, bitset<3000>(0));
    int cmin = INT_MAX;
    int cmax = 0;
    for (int i = 0; i < n; i++) {
        int c, d; cin >> c >> d;
        cmin = min(cmin, c);
        cmax = max(cmax, d);
        for (int j = c; j <= d; j++)
            client[i][j] = true;
    }
    
    int m = 0;
    for (int i = 0; i <= cmax - wmin; i++) {
        int s = 0;
        for (int j = 0; j < n; j++) {
            s += (west[j] & (client[j] >> i)).count();  
        }
        m = max(m, s);
    }
    for (int i = 0; i <= wmax - cmin; i++) {
        int s = 0;
        for (int j = 0; j < n; j++) {
            s += (west[j] & (client[j] << i)).count();  
        }
        m = max(m, s);
    }
    
    cout << m << "\n";

    return 0;
}
