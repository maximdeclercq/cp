#include <bits/stdc++.h>
using namespace std;

int r, c, k, h, v; 
vector<int> x(305, 0);
int y[305];
int mem[610][305][305];

int solve(int k, int v, int h) {
    if (k == 0) return x[v] * y[h];
    if (k > v + h) return INT_MAX;
    if (mem[k][v][h] > 0) return mem[k][v][h];
    
    int m = INT_MAX;
    for (int i = v - 1; i >= 0; i--)
        m = min(m, max(solve(k - 1, i, h), (x[v] - x[i]) * y[h]));
    for (int i = h - 1; i >= 0; i--)
        m = min(m, max(solve(k - 1, v, i), x[v] * (y[h] - y[i])));
    
    return mem[k][v][h] = m;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    cin >> r >> c >> k >> h >> v;
    memset(x, 0, sizeof(x));
    memset(y, 0, sizeof(y));
    memset(mem, 0, sizeof(mem));
    for (int i = 0; i < h; i++) cin >> y[i];
    for (int i = 0; i < v; i++) cin >> x[i];
    y[h] = r;
    x[v] = c;

    cout << solve(k, v, h) << "\n";

    return 0;
}
