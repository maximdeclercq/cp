#include <bits/stdc++.h>
using namespace std;

#define PB(i) push_back(i)
#define MP(i, j) make_pair((i), (j))
#define F(i) (i).first
#define S(i) (i).second

const int N = 1e5 + 10;
vector<pair<int, int>> delta;
int a[N];
int b[N];
int c[N]; 
int d[N];

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    
    int n; cin >> n;
    for (int i = 0; i < n; i++) cin >> a[i] >> b[i];
    for (int i = 0; i < n; i++) cin >> c[i] >> d[i];
    
    for (int i = 0; i < n; i++) {
        delta.PB(MP(a[i] - d[i], 1));
        delta.PB(MP(a[i] - c[i], -1));
        delta.PB(MP(b[i] - d[i], -1));
        delta.PB(MP(b[i] - c[i], 1));
    }
    
    sort(delta.begin(), delta.end());
    int cury = 0, maxy = 0, curs = 0, prex = INT_MIN;
    for (int i = 0; i < 4 * n; i++) {
        cout << F(delta[i]) << ":" << S(delta[i]) << "\n";
    }
    
    return 0;
}
