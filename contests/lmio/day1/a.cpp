#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair
#define F first
#define S second

const int N = 20000;

vector<int> trains[N];
vector<int> busses[N];
bitset<N> visited;
int deg[N];

int dfs(int a, int t = 1) {
	visited[a] = true;
	int sum = 1;
	if (t > 0) {
		for (int b : busses[a]) {
			if (!visited[b]) {
				sum += dfs(b, t - 1);
				visited[b] = true;
			}
		}
	}
	for (int b : trains[a]) {
		if (!visited[b]) {
			sum += dfs(b, t);
		}
	}
	if (t > 0) {
		for (int b : busses[a]) {
			visited[b] = false;
		}
	}
	visited[a] = false;
	return sum;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    int n, m; cin >> n >> m;
    
	memset(deg, 0, sizeof(deg));
	
	int x = 0;
	int y = 0;
    for (int i = 0; i < m; i++) {
    	int a, b; char c; cin >> a >> b >> c; a--; b--;
    	if (c == 'T') {
    		x++;
    		//trains[a].PB(b);
    		//trains[b].PB(a);
    	}
    	else {
    		y++;
    		//busses[a].PB(b);
    		//busses[b].PB(a);
    	}
    }
    if (x == 0) {
    	cout << "0\n"; return 0;
    }
    if (y == 0) {
    	cout << x << "\n"; return 0;
    }
    
    int r = 0, ma = 0;
    for (int i = 0; i < n; i++) {
    	int k = deg[i] = dfs(i);
    	if (k > ma) {
			ma = k;
			r = 1;
		}
		else if (k == ma) {
			r++;
		}
    }
    
    /*for (int i = 0; i < n; i++) {
    	cout << i + 1 << " = " << deg[i] << "\n";
    } */ 	
    
    cout << r << "\n";

    return 0;   
}
