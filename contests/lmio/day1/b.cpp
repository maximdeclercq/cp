#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair
#define MT make_tuple
#define F first
#define S second

const uint64_t N = 300000;
const uint64_t M = 500000;

int64_t n, m;

int64_t profit[N];
vector<pair<int64_t, int64_t>> routes[N];
bitset<N> visited;

int64_t z = 0;
void dfs(int64_t i) {
	for (auto j : routes[i]) {
		if (j.F == i) continue;
		for (auto k : routes[j.F]) {
			if (k.F == i) continue;
			for (auto l : routes[k.F]) {
				if (l.F == i) {
					profit[i] = max(profit[i], (int64_t)(j.S + k.S + l.S));
					z = max((int64_t)profit[i], (int64_t)z);
				}
			}
		}
	}
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    cin >> n >> m;
    
    fill(profit, profit + n, 0);
    /*int64_t s = sqrt(m);
    for (int64_t i = 0; i < n; i++) {
    	routes[i].reserve(s);
    }*/
    
    for (int64_t i = 0; i < m; i++) {
    	int a, b, p; cin >> a >> b >> p; a--; b--;
    	profit[a] += p;
    	profit[b] += p;
    	routes[a].PB(MP(b, p));
    	routes[b].PB(MP(a, p));
    }
    
    for (int64_t i = 0; i < n; i++) {
    	dfs(i);
    }
    
    cout << z << "\n";

    return 0;
}
