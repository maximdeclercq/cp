#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair
#define F first
#define S second

const int64_t N = 50;

uint64_t n;
pair<uint64_t, uint64_t> uint64_t x[N];

void solve(int i = 0, int j = 18, string instr = "") {
	uint64_t p = x[0].F;
	uint64_t q = x[0].S % pow(10, j) / pow(10, i);
	for (int a = 0; a <= q / p; a++) {
		for (int b = 0; b <= p; b++) {
			if (a * p + b == q) {
				for (int k = 1; k < n; k++) {
					for (int l = 1; l <= 18; l++) {
						if (a * x[k].F + b == x[k].S / pow(10, l)) {
							string ns = instr;
							if (a != 1) instr += "multiply " + to_string(a) + "\n";
							if (b != 0) instr += "add " + to_string(b) + "\n";
							solve(
						}
					}
				} 
			}
		}
	}
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    cin >> n;
    for (uint64_t i = 0; i < n; i++) {
    	uint64_t a, b; cin >> a >> b;
    	x[i] = MP(a, b);
    }
    
    if (n == 1) {
    	uint64_t add = b[0] - a[0];
    	cout << ((b[0] > a[0]) + (b[0] < a[0]) + (b[0] < a[0] && b[0] != 0) + 1) << "\n";
    	if (b[0] > a[0]) cout << "add " << add << "\n";
    	if (b[0] < a[0]) cout << "multiply 0\n";
    	if (b[0] < a[0] && b[0] != 0) cout << "add " << b[0] << "\n";
    	cout << "print\n";
    }
    
	//solve(0, "", x, new string[N]());
    return 0;   
}
