#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair
#define F first
#define S second

const int N = 1000000;
int n, a, b;
int t[N];
int d[N];

/*int solve(int x) {
	if (d[x] < INT_MAX) return d[x];
	int l = a;
	int r = min(n - x, b);
	
	while (true) {
		int m = t[x + i - 1] - t[x];
		if (m >= d[x]) break;
		if (x + i < n)
			m = max(m, solve(x + i));
		d[x] = min(d[x], m);
	}
	return d[x];
}*/

int solve(int x) {
	if (d[x] < INT_MAX) return d[x];
	int l = a;
	int r = min(n - x, b);
	for (int i = l; i <= r; i++) {
		int m = t[x] - t[x + i - 1];
		if (m >= d[x]) break;
		if (x + i < n)
			m = max(m, solve(x + i));
		d[x] = min(d[x], m);
	}
	return d[x];
}
    
int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    cin >> n >> a >> b;
    for (int i = 0; i < n; i++) {
    	cin >> t[i];
    }
    sort(t, t + n, greater<int>());
    fill(d, d + n, INT_MAX);
    
    cout << solve(0) << "\n";
    
    return 0;   
}
