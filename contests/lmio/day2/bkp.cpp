#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair
#define F first
#define S second

const uint64_t N = 50;

uint64_t n;
uint64_t x[N];
string a[N];
string b[N];

uint64_t best = ULLONG_MAX;
string best_instr = "";

bool solve(uint64_t num, string instr, uint64_t xt[N], string bt[N]);

void mul(uint64_t num, string instr, uint64_t xt[N], string bt[N]) {
	if (num >= best) return;
	
	for (uint64_t i = 0; i < 1e18; i++) {
		for (uint64_t j = 0; j < n; j++) {
			xt[j] *= i;
		}
		if (solve(num + 1, instr + "multiply" + to_string(i) + "\n", xt, bt)) {
			break;
		}
	}
}

void add(uint64_t num, string instr, uint64_t xt[N], string bt[N]) {
	if (num >= best) return;
	
	for (uint64_t i = 1; i < 1e18; i++) {
		for (uint64_t j = 0; j < n; j++) {
			xt[j] += i;
		}
		if (solve(num + 1, instr + "add" + to_string(i) + "\n", xt, bt)) {
			break;
		}
	}
}

void print(uint64_t num, string instr, uint64_t xt[N], string bt[N]) {
	if (num >= best) return;
	
	for (uint64_t i = 0; i < n; i++) {
		bt[i] += to_string(xt[i]);
	}
	
	solve(num + 1, instr + "print\n", xt, bt);
}

bool solve(uint64_t num, string instr, uint64_t xt[N], string bt[N]) {
	if (num >= best) return true;
	
	uint64_t i;
	for (i = 0; i < n; i++) {
		if (b[i] != bt[i]) break;
	}
	if (i == n) {
		best = num;
		best_instr = instr;
		return true;
	}
	
	print(num, instr, xt, bt);
	mul(num, instr, xt, bt);
	add(num, instr, xt, bt);
	return false;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    cin >> n;
    for (uint64_t i = 0; i < n; i++) {
    	cin >> a[i] >> b[i];
    	x[i] = stoi(a[i]);
    }
    
	solve(0, "", x, new string[N]());
    return 0;   
}
