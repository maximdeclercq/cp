#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair
#define F first
#define S second

const int N = 1000;

int t, n, s;
vector<int> graph[N];
vector<int> toposort;
bitset<N> visited;

void dfs(int x) {
	if (visited[x]) return;
	visited[x] = true;
	for (int y : graph[x])
		dfs(y);
	toposort.PB(x);
}

void reset(int n) {
	for (int i = 0; i < n; i++) {
		graph[i].clear();
	}
	toposort.clear();
	visited.reset();
	fill(deg, deg + n, 0);
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
	cin >> t >> n >> s;
	reset(n);
	for (int i = 0; i < n; i++) {
		int k; cin >> k;
		for (int j = 0; j < k; j++) {
			int l; cin >> l; l--;
			graph[l].PB(i);
		}
	}
	
	for (int i = 0; i < n; i++) {
		dfs(i);
	}
	for (int i = n - 1; i >= 0; i--) {
		cout << toposort[i] + 1 << "\n";
	}
    
    
    
    return 0;   
}
