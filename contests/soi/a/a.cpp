#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define int int64_t

int n, k, q; 

bool query(int l, int r) {
	cout << "q " << l << " " << r << "\n";
	cout.flush();
	char a; cin >> a;
	return a == 'y';
}

int cnt = 0;
V<int> answer;
/*void solve(int l, int r) {
	if (l == r || cnt == k) return;
	int m = (l + r) / 2;
	bool lq = l != m && query(l, m);
	bool rq = m + 1 != r && query(m + 1, r);
	
	if (!lq && !rq) {
		cnt++, answer.PB(m);
	}
	else {
		if (lq)
			solve(l, m);
		if ((!lq && m + 1 == r) || query(m, m + 1))
			cnt++, answer.PB(m);
		if (rq)
			solve(m + 1, r);
	}
}*/

int solve(int l, int r) {
	int m = (l + r) / 2;
	if (query(m, m + 1))
		cnt++, answer.PB(m);
	
}

int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	cin >> n >> k >> q;
	
	solve(0, n - 2);
	
	cout << "a";
	for (int a : answer)
		cout << " " << a;
	cout << "\n";
	
    return 0;
}
