#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define int int64_t

pair<int, int> a[50000];

int n, k;
int best[2500];
int best = INT64_MAX;

int solve(int x = 0, int cnt = 0, int A = 0, int B = 0, int sum = 0) {
	if (sum + A * B >= best[x] || sum + A * B >= best) return INT64_MAX;
	if (x >= n) return INT64_MAX;
	if (x > n - (k - cnt - 1)) return INT64_MAX;
	if (cnt > k) return INT64_MAX;
	A += a[x].F;
	B += a[x].S;
	if (cnt == k - 1) {
		for (int i = x + 1; i < n; i++) {
			A += a[i].F;
			B += a[i].S;
		}
		return best = min(best, sum + A * B);
	}
	return best[x] = min(best[x], min(
		solve(x + 1, cnt + 1, 0, 0, sum + A * B),
		solve(x + 1, cnt, A, B, sum)
	));
}

int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	fill(best, best + 2500, INT64_MAX);
	cin >> n >> k;
	for (int i = 0; i < n; i++) {
		int p, q; cin >> p >> q;
		a[i] = MP(p, q);
	}
	cout << solve() << "\n";
	
    return 0;
}
