#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define int int64_t

int parent[100007];
V<int> child[100007];
int cost[100007];
int dist[100007];

int count(int x) {
	if (dist[x] >= 0)
		return dist[x];
	return dist[x] = count(parent[x]) + cost[x];
}

void updateDelta(int x, int d) {
	dist[x] += d;
	for (int y : child[x])
		updateDelta(y, d);
}

void update(int x, int c) {
	updateDelta(x, c - cost[x]);
	cost[x] = c;
}


int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	int n, q; cin >> n >> q;
	dist[0] = 0;
	for (int i = 1; i < n; i++) {
		int v, c; cin >> v >> c;
		parent[i] = v;
		cost[i] = c;
		child[v].PB(i);
		dist[i] = -1;
	}
	
	for (int i = 0; i < n; i++)
		count(i);
	
	for (int i = 0; i < q; i++) {
		char ch; int a, c; cin >> ch >> a;
		if (ch == 'u') {
			cin >> c;
			update(a, c);
		}
		else if (ch == 'q') {
			cout << dist[a] << "\n";
		}
	}
	
    return 0;
}
