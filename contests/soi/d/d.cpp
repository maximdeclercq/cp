#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

#define int int64_t

int a[1000007];

int32_t main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	int n, j; cin >> n >> j;
	int sum = 0;
	for (int i = 0; i < n; i++) {
		cin >> a[i];
		sum += a[i];
	}
	
	if (n <= 3) {
		int sum = (j >= 2 ? n + 1 : j);
		for (int i = 0; i < (j >= 2 ? n : j); i++)
			sum += a[i];
		cout << sum << "\n";
	}
		
	
	
    return 0;
}
