#include <bits/stdc++.h>
using namespace std;

#define LSO(i) ((i) & (-i))
vector<int> ft;

int query(int i) {
    int sum = 0;
    for(; i > 0; i -= LSO(i))
        sum += ft[i];
    return sum;
}

int query(int i, int j) {
    return query(j) - query(i - 1);
}

int n;
void update(int i, int v) {
    for(; i <= n; i += LSO(i))
        ft[i] += v;
}

set<int> splits;
void split(int i) {
    splits.insert(i);
}
 
int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    cin >> n;

    ft.resize(2 * n);

    for (int i = 1; i <= n; i++) {
        int v;
        cin >> v;
        
        update(i, v);
    }
    
    splits.insert(1);
    splits.insert(n);


    for (int i = 1; i <= n; i++) {
        int v;
        cin >> v;

        split(v);
        update(v, -query(v, v));

        int m = 0;
        for (auto it = splits.begin(); it != splits.end();) {
            int a = *it;
            if (it++ != splits.end()) {
                int q = query(a, *it);
                m = max(q, m);
            }
        } 
        cout << m << "\n";
    }

    return 0;
}