#include <bits/stdc++.h>
using namespace std;

const int MOD = pow(10, 9) + 7;

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    uint32_t numGyms, numTypes;
    cin >> numGyms >> numTypes;

    vector<vector<uint32_t>> types(numTypes);

    for (uint32_t i = 0; i < numGyms; i++) {
        uint32_t numPoks;
        cin >> numPoks;
        for (uint32_t j = 0; j < numPoks; j++) {
            uint32_t type;
            cin >> type;
            types[--type].push_back(i);
        }
    }

    sort(types.begin(), types.end());

    uint64_t numPerms = 1;
    uint64_t counter = 1;
    for (uint32_t i = 1; i < (uint32_t)types.size(); i++) {
        if (types[i] == types[i - 1]) {
            counter++;
        }
        else {
            counter = 1;
        }

        numPerms *= counter;
        numPerms %= MOD;
    }
    cout << numPerms << "\n";

    return 0;
}