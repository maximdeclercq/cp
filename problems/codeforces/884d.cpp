#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    int n; cin >> n;
    priority_queue<int, vector<int>, greater<int>> q;
    for (int i = 0; i < n; i++) {
    	int a; cin >> a;
    	q.push(a);
    }
    
    int penalty = 0;
    while (q.size() > 1) {
    	if (q.size() % 2 != 0) {
			int a = q.top(); q.pop();
			int b = q.top(); q.pop();
			int c = q.top(); q.pop();
			penalty += a + b + c;
			q.push(a + b + c);
    	}
    	else {
			int a = q.top(); q.pop();
			int b = q.top(); q.pop();
			penalty += a + b;
			q.push(a + b);
    	}
    }
    
    cout << penalty << "\n";
    
    return 0;
}
