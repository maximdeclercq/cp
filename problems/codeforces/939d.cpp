 #include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair

char a[100007];
char b[100007];

int graph[26][26];
bitset<26> visited;

vector<pair<char, char>> res;

void dfs(int u, int p = -1) {
	visited[u] = true;
	for (int v = 0; v < 26; v++) {
		if (graph[u][v] && !visited[v]) {
			res.PB(MP(v + 'a', u + 'a'));
			dfs(v, u);
		}
	}
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(0);

	int n; cin >> n;
	for (int i = 0; i < n; i++)
		cin >> a[i];
	for (int i = 0; i < n; i++)
		cin >> b[i];
		
	for (int i = 0; i < n; i++) {
		graph[a[i] - 'a'][b[i] - 'a'] = true;
		graph[b[i] - 'a'][a[i] - 'a'] = true;
	}

	for (int i = 0; i < 26; i++) {
		if (!visited[i]) {
			dfs(i);
		}
	}

	cout << res.size() << "\n";
	for (auto a : res) {
		cout << a.first << " " << a.second << "\n";
	}

	return 0;
}

