#include <bits/stdc++.h>
using namespace std;

#define PB push_back

vector<vector<int>> adj;
vector<int> color;

bool dfs(int a) {
    for (int b : adj[a]) {
        if (color[b] == -1) {
            color[b] = !color[a];
            if (!dfs(b)) return false;
        }
        else if (color[a] == color[b]) {
            return false;
        }
    }
    
    return true;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    while (true) {
        int n; cin >> n;
        if (n == 0) return 0;
        int l; cin >> l;
        
        adj.clear();
        adj.assign(n, vector<int>());
        color.clear();
        color.assign(n, -1);
        
        for (int i = 0; i < l; i++) {
            int a, b; cin >> a >> b;
            adj[a].PB(b);
            adj[b].PB(a);
        }
        
        color[0] = 0;
        cout << (dfs(0) ? "BICOLORABLE." : "NOT BICOLORABLE.") << "\n";
    }
       
    return 0;
}
