#include <bits/stdc++.h>
using namespace std;

int n, total_weight;
int w[107];
int possible[45007];

int solve() {
	possible[0] = true;
	for (int k = 1; k <= n; k++) {
		for (int x = total_weight - w[k]; x >= 0; x--) {
			possible[x + w[k]] |= possible[x];
		}
	}
	
	int min_diff = INT_MAX;
	for (int k = 0; k <= total_weight; k++) {
		if (possible[k])
			min(min_diff, abs(k - (total_weight - k)));
	}
	return min_diff;
}

void reset() {
	total_weight = 0;
	memset(possible, false, sizeof(possible));
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    
    int t; cin >> t;
    while (t--) {
    	reset();
    	
    	cin >> n;
    	for (int i = 1; i <= n; i++) {
    		cin >> w[i];
    		total_weight += w[i];
    	}
    		
    	int diff = solve();	
    	
 		int a = (total_weight - diff) / 2;
 		int b = total_weight - a;
 		
 		cout << a << " " << b << "\n\n";
    }
    
    return 0;
}

