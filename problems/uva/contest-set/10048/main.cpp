#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair
#define F first
#define S second
#define P pair
#define V vector

int p[100];

void make_set(int x) {
	p[x] = x;
}

int find_set(int x) {
	if (p[x] != x)
		p[x] = find_set(p[x]);
	return p[x];
}

void merge_sets(int x, int y) {
	int xs = find_set(x);
	int ys = find_set(y);
	p[ys] = xs;
}

V<P<int, int>> mst[100];
bitset<100> visited;
int path = 1e9;
void minimax(int s, int t, int m = 0) {
	visited[s] = true;
	if (s == t) path = min(m, path);
	for (P<int, int> a : mst[s]) {
		if (!visited[a.F])
			minimax(a.F, t, max(a.S, m));
	}
}


void reset() {
	for (int i = 0; i < 100; i++) {
		mst[i].clear();
		make_set(i);
	}
}

int main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	int t = 0;
	while (true) {
		int c, s, q; cin >> c >> s >> q;
		if (c == 0 && s == 0 && q == 0) break;
		if (t != 0) cout << "\n";
		reset();
		
		V<P<int, P<int, int>>> edges;
		for (int i = 0; i < s; i++) {
			int a, b, c; cin >> a >> b >> c; a--; b--;
			edges.PB(MP(c, MP(a, b)));
		}
		sort(edges.begin(), edges.end());
		
		for (int i = 0; i < s; i++) {
			int c = edges[i].F;
			P<int, int> e = edges[i].S;
			if (find_set(e.F) != find_set(e.S)) {
				merge_sets(e.F, e.S);
				mst[e.F].PB(MP(e.S, c));
				mst[e.S].PB(MP(e.F, c));
			}
		}
		
		cout << "Case #" << ++t << "\n";
		for (int i = 0; i < q; i++) {
			int a, b; cin >> a >> b; a--; b--;
			visited.reset();
			path = 1e9;
			minimax(a, b);
			if (path == 1e9)
				cout << "no path\n";
			else
				cout << path << "\n";
		}			
	}
		
    return 0;
}
