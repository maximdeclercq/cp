#include <bits/stdc++.h>
using namespace std;

#define N 1007
#define W 37
#define PB push_back
#define MP make_pair

int n, g, mw;

pair<int, int> objects[N]; // w, p

int memo[N][W];

int solve(int i = 0, int w = 0) {
	if (w > mw) return INT_MIN;
	if (i >= n) return 0;
	
	if (memo[i][w] >= 0) return memo[i][w];
	
	return memo[i][w] = max(solve(i + 1, w), objects[i].second + solve(i + 1, w + objects[i].first));
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    int t; cin >> t;
    while (t--) { 
    	cin >> n;
    	
    	for (int i = 0; i < n; i++) {
    		int a, b; cin >> a >> b;
    		objects[i] = MP(b, a);
    	}
    	
    	sort(objects, objects + n, greater<pair<int, int>>());
    		
    	cin >> g;
    	int r = 0;
    	for (int i = 0; i < g; i++) {
    		cin >> mw;
    		memset(memo, -1, sizeof(memo));  
    		r += solve();
    	}
    	cout << r << "\n";
    }

    return 0;
}
