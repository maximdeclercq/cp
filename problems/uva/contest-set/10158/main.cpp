#include <bits/stdc++.h>
using namespace std;

vector<int> friends;
vector<int> enemies;

// Union Find: create country x with no friends nor enemies
void createCountry(int x) {
    friends[x] = x;
    enemies[x] = -1;
}

// Union Find : find the 'root' friend of x
int findFriend(int x) {
    if (x < 0) return x;

    if (friends[x] != x)
        friends[x] = findFriend(friends[x]);
    return friends[x];
}

// x and y are friends if they have the same 'root' friend (x ~ z && y ~ z => x ~ z)
bool areFriends(int x, int y) {
    int xFriend = findFriend(x);
    int yFriend = findFriend(y);

    if (xFriend == -1 || yFriend == -1) return false;

    return xFriend == yFriend;
}

// x and y are enemies if x is an enemy of y or y is an enemy of x, (x * z && y ~ z => x * y) || (x ~ z && y * z => x * y)
bool areEnemies(int x, int y) {
    int xFriend = findFriend(x);
    int yFriend = findFriend(y);

    if (xFriend == -1 || yFriend == -1) return false;
    
    return enemies[xFriend] == yFriend || xFriend == enemies[yFriend];
}

// Union Find : merge two sets of friends, BUT see comments for special cases
int makeFriends(int x, int y) {
    if (areEnemies(x, y)) return -1;
    if (areFriends(x, y)) return 0;

    int xFriend = findFriend(x);
    int yFriend = findFriend(y);

    if (xFriend == -1 || yFriend == -1) return -1;

    // make x the root node
    friends[yFriend] = xFriend;

    // x * a && x * b => a ~ b 
    if (enemies[xFriend] != -1 && enemies[yFriend] != -1)
        makeFriends(enemies[xFriend], enemies[yFriend]);
    // x ~ y && y * z => x * z
    else if (enemies[yFriend] != -1)
        enemies[xFriend] = findFriend(enemies[yFriend]);
    return 1;
}

// Union Find : merge x with enemy of y and merge enemy of x with y
int makeEnemies(int x, int y) {
    if (areFriends(x, y)) return -1;
    if (areEnemies(x, y)) return 0;

    int xFriend = findFriend(x);
    int yFriend = findFriend(y);

    if (xFriend == -1 || yFriend == -1) return -1;

    // x * y && x * z => y ~ z
    if (enemies[xFriend] != -1)
        makeFriends(enemies[xFriend], yFriend);
    enemies[xFriend] = findFriend(yFriend);
    // x * y && y * z => x ~ z
    if (enemies[yFriend] != -1)
        makeFriends(xFriend, enemies[yFriend]);
    enemies[yFriend] = findFriend(xFriend);
    return 1;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int n;
    cin >> n;

    friends.resize(n);
    enemies.resize(n);

    for (int i = 0; i < n; i++) {
        createCountry(i);
    }

    while (1) {
        int c, x, y;
        cin >> c >> x >> y;

        if (c == 0 && x == 0 && y == 0) break;

        switch (c) {
            case 1:
                if (makeFriends(x, y) == -1) cout << -1 << "\n";
                break;
            case 2:
                if (makeEnemies(x, y) == -1) cout << -1 << "\n";
                break;
            case 3:
                cout << (areFriends(x, y) ? 1 : 0) << "\n";
                break;
            case 4:
                cout << (areEnemies(x, y) ? 1 : 0) << "\n";
                break;
        }
    }
}

