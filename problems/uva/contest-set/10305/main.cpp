#include <bits/stdc++.h>
using namespace std;

#define PB push_back

vector<vector<int>> adj;
vector<int> toposort;
bitset<105> visited;

void dfs(int a) {
    if (visited[a]) return;
    visited[a] = true;
    
    for (int b : adj[a])
        dfs(b);
    
    toposort.PB(a);
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    while (true) {
        int n, m; cin >> n >> m;
        if (n == 0 && m == 0) return 0;
        
        adj.clear();
        adj.assign(n + 1, vector<int>());
        toposort.clear();
        visited.reset();
        
        for (int i = 0; i < m; i++) {
            int a, b; cin >> a >> b;
            adj[a].PB(b);
        }
        
        for (int i = 1; i <= n; i++)
            dfs(i);
            
        for (int i = n - 1; i >= 0; i--)
            cout << toposort[i] << (!i ? "\n" : " ");
    }
       
    return 0;
}
