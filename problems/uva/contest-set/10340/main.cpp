#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    
    while (!cin.eof()) {
        string a = "";
        string b = "";

        cin >> a >> b;

        if (b.length() < a.length()) {
            cout << "No" << endl;
            continue;
        }
        else if (b.length() == 0 || a.length() == 0) {
            break;
        }

        int ia = 0;
        for (int ib = 0; ib < b.length(); ib++) {
            if (a[ia] == b[ib]) {
                ia++;
            }
        }

        if (ia == a.length()) {
            cout << "Yes" << endl;
        }
        else {
            cout << "No" << endl;
        }
    }

    return 0;
}