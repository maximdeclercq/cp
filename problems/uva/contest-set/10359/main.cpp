#include <bits/stdc++.h>
using namespace std;

struct BigInt {
    vector<int> d;

    // a < 10
    BigInt(int a = 0) {
        d.push_back(a % 10);
    }

    BigInt operator+(const BigInt & b) const {
        BigInt result = b;

        for (int i = 0, carry = 0; i < (int)max(d.size(), b.d.size()) || carry; i++) {
            if (i == (int)result.d.size())
                result.d.push_back(0);
            result.d[i] += carry + (i < (int)d.size() ? d[i] : 0);
            carry = result.d[i] >= 10;
            if (carry)
                result.d[i] -= 10;
        }

        return result;
    }

    bool operator==(const BigInt & b) const {
        return d == b.d;
    }

    bool operator!=(const BigInt & b) const {
        return d != b.d;
    }

    void print() {
        if ((int)d.size() == 0) {
            cout << "0\n";
            return;
        }
        for (int i = (int)d.size() - 1; i >= 0; i--) {
            cout << d[i];
        }
        cout << "\n";
    }
};

map<int, BigInt> mem;

BigInt numWays(int n) {
    if (n == 0) return BigInt(1);
    if (n == 1) return BigInt(1);
    if (mem[n] != BigInt(0)) return mem[n];
    return mem[n] = numWays(n - 1) + numWays(n - 2) + numWays(n - 2);
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int n; 
    while (cin >> n) {
        numWays(n).print();
    }

    return 0;
}