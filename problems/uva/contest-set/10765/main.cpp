#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair
#define N 10000

int t, rC;

vector<int> graph[N];
bitset<N> visited;

int num[N];
int low[N];

pair<int, int> ap[N]; // -pigeon value, index 

void findAp(int a, int p = -1) {
	num[a] = low[a] = t++;
	visited[a] = true;
	for (int b : graph[a]) {
		if (!visited[b]) {
			findAp(b, a);
			if (p < 0) rC++;
			else if (low[b] >= num[a]) ap[a].first--;
			low[a] = min(low[a], low[b]);
		}
		else if (b != p) {
			low[a] = min(low[a], num[b]);
		}
	}
}

void reset() {
	t = rC = 0;
	for (int i = 0; i < N; i++) graph[i].clear(), ap[i] = MP(-1, i);
	visited.reset();
	fill(num, num + N, 0);
	fill(low, low + N, 0);
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
	
	while (true) {
		reset();
	
		int n, m; cin >> n >> m;
		if (n == 0 && m == 0) break;
		
		while (true) {
			int x, y; cin >> x >> y;
			if (x == -1 && y == -1) break;
			
			graph[x].PB(y);
			graph[y].PB(x);
		}
		
		for (int i = 0; i < n; i++) {
			if (!visited[i]) {
				rC = 0;
				findAp(i);
				if (rC > 1) ap[i].first--;
			}
		}
		
		sort(ap, ap + n);
		for (int i = 0; i < m; i++)
			cout << ap[i].second << " " << -ap[i].first << "\n";
		cout << "\n";
	}
	
	return 0;
}

