#include <bits/stdc++.h>
using namespace std;

int m;
string e;
vector<pair<char, string>> a;

void solve(int p = 0, string o = "") {
    if (p == (int)e.size()) {
        cout << o << "\n";
        m++;
        return;
    }

    for (int i = 0; i < (int)a.size(); i++) {
        if (m >= 100) break;
        int k = e.find(a[i].second, p);
        if (k > -1 && k < (int)e.size()) {
            bool success = true;
            for (int j = p; j < k; j++) {
                success = success && (e[j] == '0');
                if (success == false) break;
            }
            if (success == false) continue;
            solve(k + a[i].second.size(), o + a[i].first);
        }
    }
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    
    int c = 0;
    while (true) {
        int n; cin >> n;
        if (n == 0) break;
        
        m = 0;       
        
        a.clear();
        for (int i = 0; i < n; i++) {
            char x; int y; cin >> x >> y;
            a.push_back(make_pair(x, to_string(y)));
        }
        sort(a.begin(), a.end());
        
        cin >> e;
        
        c++;
        cout << "Case #" << c << "\n";
        solve();
        cout << "\n";
    }    
    
    return 0;
}

