#include <iostream>
using namespace std;


int main() {
    ios_base::sync_with_stdio(false);
    
    int t = 0;
    cin >> t;

    for (int i = 0; i < t; i++) {
        int a = 0;
        int b = 0;
        
        cin >> a >> b;

        if (a < b) {
            cout << '<' << endl;
        }
        else if (a > b) {
            cout << '>' << endl;
        }
        else {
            cout << '=' << endl;
        }
    }
    return 0;
}