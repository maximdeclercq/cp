#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair
#define F first
#define S second

int p[1000];

void make_set(int x) {
	p[x] = x;
}

int find_set(int x) {
	if (p[x] != x)
		p[x] = find_set(p[x]);
	return p[x];
}

void merge_sets(int x, int y) {
	int xs = find_set(x);
	int ys = find_set(y);
	p[ys] = xs;
}

int main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);

    int t; cin >> t;
    for (int c = 0; c < t; c++) {
        int n, r; cin >> n >> r;

        vector<pair<int, int>> coords;
        for (int i = 0; i < n; i++) {
            int x, y; cin >> x >> y;
            coords.PB(MP(x, y));
        }
        
        vector<pair<int, pair<int, int>>> edges;
        for (int i = 0; i < n; i++) {
        	for (int j = i + 1; j < n; j++) {
        		int a = coords[i].F;
        		int b = coords[i].S;
        		int x = coords[j].F;
        		int y = coords[j].S;
        		edges.PB(MP((x - a) * (x - a) + (y - b) * (y - b), MP(i, j)));
        	}
        }
        
        for (int i = 0; i < n; i++) {
        	make_set(i);
        }
        
        int s = 1;
        double rds = 0;
        double rws = 0;
        sort(edges.begin(), edges.end());
        for (int i = 0; i < edges.size(); i++) {
        	int c = edges[i].F;
    		pair<int, int> e = edges[i].S;
        	if (find_set(e.F) != find_set(e.S)) {
        		if (c <= r * r)
        			rds += sqrt(c);
        		else
        			rws += sqrt(c), s++;
        		merge_sets(e.F, e.S);
        	}
        }
        
        cout << "Case #" << c + 1 << ": " << s << " " << round(rds) << " " << round(rws) << "\n";
        
        
	}

    return 0;
}
