#include <bits/stdc++.h>
using namespace std;

struct Node {
    Node * l;
    Node * r;
    int lb, rb; // bounds
    int val;
};

vector<pair<int, int>> indices;

/* Build tree on [i, j] */
Node * build(int i, int j) {
    if (i == j) {
        return new Node { nullptr, nullptr, indices[i].first, indices[i].second, indices[i].second - indices[i].first + 1 };
    }
    else {
        Node * l = build(i, (i + j) / 2);
        Node * r = build((i + j) / 2 + 1, j);
        return new Node { l, r, l->lb, r->rb, max(l->val, r->val) };
    }
}

int query(Node * cur, int lb, int rb) {
    // All values are the same
    if (cur->l == nullptr) {
        return min(cur->rb, rb) - max(cur->lb, lb) + 1;
    }

    // ignore
    if (cur->rb < lb || cur->lb > rb) {
        return 0;
    }
    else if (cur->lb >= lb && cur->rb <= rb) {
        return cur->val;
    }
    else {
        int lVal = query(cur->l, lb, rb);
        int rVal = query(cur->r, lb, rb);
        return max(lVal, rVal);
    }
}

int main() {
    ios_base::sync_with_stdio(false);

    while (1) {
        int n, q;
        cin >> n;

        if (n == 0) break;

        cin >> q;

        indices.clear();

        int count = 1;
        int last = INT_MAX;
        for (int i = 0; i < n; i++) {    
            int a;  
            cin >> a;
            if (a == last || last == INT_MAX) {
                count++;
            }
            else {
                indices.push_back(make_pair(i - count, i - 1));
                count = 1;
            }
            last = a;
        }
        indices.push_back(make_pair(n - count, n - 1));
        
        Node * root = build(0, indices.size() - 1);

        for (int i = 0; i < q; i++) {
            int x, y;
            cin >> x >> y;
            x--; y--;
            cout << query(root, x, y) << "\n";
        }
    }
    cout.flush();
}
