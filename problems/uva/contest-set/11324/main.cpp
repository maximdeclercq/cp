#include <bits/stdc++.h>
using namespace std;

#define PB push_back

#define N 1000

int n, m;
vector<int> graph[N];
vector<int> graphT[N];
bitset<N> visited;

vector<int> toposort;

int numSccs;
vector<int> sccs[N];
int sccSize[N];
int nodeToScc[N];

void findToposort(int u) {
	if (visited[u]) return;
	visited[u] = true;
	for (int v : graph[u])
		findToposort(v);
	toposort.PB(u);
}

void findScc(int u, int a) {
	if (visited[u]) return;
	visited[u] = true;
	for (int v : graphT[u])
		findScc(v, a);
	sccs[a].PB(u);
	sccSize[a]++;
	nodeToScc[u] = a;
}

void calcSccs() {
	toposort.clear();
	
	visited.reset();
	for (int i = 0; i < n; i++)
		findToposort(i);

	numSccs = 0;
	for (int i = 0; i < n; i++)
		sccs[i].clear();
	memset(sccSize, 0, sizeof(sccSize));
	
	visited.reset();
	for (int i = n - 1; i >= 0; i--) {
		if (!visited[toposort[i]])
			findScc(toposort[i], numSccs++);
	}
}

int findClique(int a) {
	if (visited[a]) return 0;
	visited[a] = true;
	int m = sccSize[a];
	for (int u : sccs[a]) {
		for (int v : graph[u]) {
			int b = nodeToScc[v];
			m = max(m, findClique(b) + sccSize[a]); 
		}
	}
	visited[a] = false;
	return m;
}

int calcMaxClique() {
	int m = 0;
	visited.reset();
	for (int i = 0; i < numSccs; i++)
		m = max(m, findClique(i));
	return m;
}

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(0);
		
	int t; cin >> t;
	while (t--) {
		cin >> n >> m;
		
		for (int i = 0; i < n; i++) 
			graph[i].clear(), graphT[i].clear();
			
		for (int i = 0; i < m; i++) {
			int x, y; cin >> x >> y; x--; y--;
			graph[x].PB(y);
			graphT[y].PB(x);
		}
		
		calcSccs();
		
		cout << calcMaxClique() << "\n";
	}
		
	return 0;
}

