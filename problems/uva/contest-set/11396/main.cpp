#include <bits/stdc++.h>
using namespace std;

#define PB push_back

vector<int> graph[301];
bitset<301> color;
bitset<301> visited;

bool dfs(int a, bool c = false) {
    if (visited[a]) return color[a] == c;
    color[a] = c;
    visited[a] = true;
    
    for (int b : graph[a])
        if (!dfs(b, !c)) return false;
    
    return true;
}

void reset() {
    for (int i = 0; i < 301; i++) graph[i].clear();
    color.reset();
    visited.reset();
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    while (true) {
        int v; cin >> v;
        if (v == 0) break;
        
        reset();
        
        while (true) {
            int a, b; cin >> a >> b;
            if (a == 0 && b == 0) break;
        
            graph[a].PB(b);
            graph[b].PB(a);
        }
        
        bool bipartite = dfs(1);
        cout << (bipartite ? "YES" : "NO") << "\n";
    }

    return 0;
}
