#include <bits/stdc++.h>
using namespace std;

string treeData;

// 0 => Ba & 1 => Bu
vector<int> tree;

// 0 => Ba & 1 => Bu & -1 => Invert 
vector<int> lazy;

// build the tree
void build(int p, int i, int j) {
    if (i == j) {
        tree[p] = treeData[i] == '1';
    }
    else {
        build(2 * p + 1, i, (i + j) / 2);
        build(2 * p + 2, (i + j) / 2 + 1, j);
        tree[p] = tree[2 * p + 1] + tree[2 * p + 2];
    }
    lazy[p] = INT_MAX;
}

// invert the lazy value by reference
void invertLazyValue(int & lazy) {
    if (lazy == INT_MAX) {
        lazy = -1;
    }
    else if (lazy == -1) {
        lazy = INT_MAX;
    }
    else {
        lazy = 1 - lazy;
    }
}

// p = array index of current node
// [i..j] = current seg
void propagate(int p, int i, int j) {
    if (lazy[p] != INT_MAX) {
        if (lazy[p] == -1) {
            tree[p] = j - i + 1 - tree[p];

            if (i != j) {
                invertLazyValue(lazy[2 * p + 1]);
                invertLazyValue(lazy[2 * p + 2]);
            }
        }
        else {
            tree[p] = (j - i + 1) * lazy[p];

            if (i != j) {
                lazy[2 * p + 1] = lazy[p];
                lazy[2 * p + 2] = lazy[p];
            }
        }
        lazy[p] = INT_MAX;
    }
}

// p = array index of current node
// [i..j] = current seg
// [a..b] = search interval
int query(int p, int i, int j, int a, int b) {
    propagate(p, i, j);

    if (j < a || i > b) {
        return 0;
    }
    else if (i >= a && j <= b) {
        return tree[p];
    }
    else {
        int m = (i + j) / 2;
        return query(2 * p + 1, i, m, a, b)
             + query(2 * p + 2, m + 1, j, a, b);
    }
}

// p = array index of current node
// [i..j] = current seg
// [a..b] = update interval
// newVal = value to set
void update(int p, int i, int j, int a, int b, int newVal) {
    propagate(p, i, j);

    if (j < a || i > b) {
        return;
    }
    else if (i >= a && j <= b) {
        lazy[p] = newVal;
        propagate(p, i, j);
    }
    else {
        int m = (i + j) / 2;
        update(2 * p + 1, i, m, a, b, newVal);
        update(2 * p + 2, m + 1, j, a, b, newVal);

        tree[p] = tree[2 * p + 1] + tree[2 * p + 2];
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int c;
    cin >> c;
    
    // Make arrays large enough
    tree.resize(10000000);
    lazy.resize(10000000);

    for (int i = 0; i < c; i++) {
        int m;
        cin >> m;

        // Populate treeData string from input
        treeData.clear();
        for (int j = 0; j < m; j++) {
            int t;
            string s;
            cin >> t >> s;

            for (int k = 0; k < t; k++) {
                treeData += s;
            }
        }

        // Build tree
        int n = treeData.size();
        build(0, 0, n);
        
        cout << "Case " << i + 1 << ":\n";

        // Number of questions
        int q;
        cin >> q;

        // Number of questions from God
        int qS = 0;

        // Process queries from input
        for (int j = 0; j < q; j++) {
            char s;
            int a, b;
            cin >> s >> a >> b;

            switch (s) {
                case 'F': // Make Bu
                    update(0, 0, n, a, b, 1);
                    break;
                case 'E': // Make Ba
                    update(0, 0, n, a, b, 0);
                    break;
                case 'I': // Invert
                    update(0, 0, n, a, b, -1);
                    break;
                case 'S': // Query
                    int out = query(0, 0, n, a, b);
                    qS++;
                    cout << "Q" << qS << ": " << out << "\n";
                    break;
            }
        }
    }
}
