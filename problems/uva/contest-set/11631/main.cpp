#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair
#define F first
#define S second
#define P pair
#define V vector

int p[200000];

void make_set(int x) {
	p[x] = x;
}

int find_set(int x) {
	if (p[x] != x)
		p[x] = find_set(p[x]);
	return p[x];
}

void merge_sets(int x, int y) {
	int xs = find_set(x);
	int ys = find_set(y);
	p[ys] = xs;
}

int main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	while (true) {
		int m, n; cin >> m >> n;
		if (n == 0 && m == 0) break;
		
		int maximum_cost = 0;
		P<int, P<int, int>> edges[n];
		for (int i = 0; i < n; i++) {
			int x, y, z; cin >> x >> y >> z;
			edges[i] = MP(z, MP(x, y));
			maximum_cost += z;
		}
		
		for (int i = 0; i < m; i++) {
			make_set(i);
		}
		
		sort(edges, edges + n);
		
		int minimum_cost = 0;
		for (int i = 0; i < n; i++) {
			int c = edges[i].F;
			P<int, int> e = edges[i].S;
			if (find_set(e.F) != find_set(e.S)) {
				merge_sets(e.F, e.S);
				minimum_cost += c;
			}
		}
		
		cout << maximum_cost - minimum_cost << "\n";
	}
	
    return 0;
}
