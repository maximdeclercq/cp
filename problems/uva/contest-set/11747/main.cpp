#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair
#define F first
#define S second
#define P pair
#define V vector

int p[1000];

void make_set(int x) {
	p[x] = x;
}

int find_set(int x) {
	if (p[x] != x)
		p[x] = find_set(p[x]);
	return p[x];
}

void merge_sets(int x, int y) {
	int xs = find_set(x);
	int ys = find_set(y);
	p[ys] = xs;
}

int main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	while (true) {
		int n, m; cin >> n >> m;
		if (n == 0 && m == 0) break;
	
		V<P<int, P<int, int>>> edges;
		for (int i = 0; i < m; i++) {
			int u, v, w; cin >> u >> v >> w;
			edges.PB(MP(w, MP(u, v)));
		}
		
		for (int i = 0; i < n; i++)
			make_set(i);
			
		V<int> heavy;
		sort(edges.begin(), edges.end());
		for (int i = 0; i < edges.size(); i++) {
			int c = edges[i].F;
			P<int, int> e = edges[i].S;
			if (find_set(e.F) != find_set(e.S)) {
				merge_sets(e.F, e.S);
			}
			else {
				heavy.PB(c);
			}
		}
		
		if (heavy.size() == 0)
			cout << "forest\n";
		
		for (int i = 0; i < heavy.size(); i++) {
			cout << heavy[i] << (i == heavy.size() - 1 ? "\n" : " ");
		}
	}
	
	
    return 0;
}
