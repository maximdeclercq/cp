#include <iostream>
using namespace std;

int main() {
    ios_base::sync_with_stdio(false);

    int t = 0;
    cin >> t;

    for (int i = 1; i <= t; i++) {
        int n = 0;
        cin >> n;

        int max = 0;
        for (int j = 0; j < n; j++) {
            int a = 0;
            cin >> a;

            if (a > max) max = a;
        }

        cout << "Case " << i << ": " << max << endl;
    }

    return 0;
}


