#include <bits/stdc++.h>
using namespace std;

map<char, int> dir { 
  { 'N', 0 }, 
  { 'S', 2 }, 
  { 'L', 1 }, 
  { 'O', 3 }
};

map<char, int> cell {
  { '.', 0 },
  { '*', 1 }, 
  { '#', 2 }
};

map<char, int> instr { 
  { 'D', 1 }, 
  { 'E', -1 },
  { 'F', 0 }
};

vector<vector<int>> grid (100, vector<int>(100));


int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    while (true) {
        int n, m, s; cin >> n >> m >> s;
        if (n == 0 && m == 0 && s == 0) break;
        
        int x = 0, y = 0, d = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                char a; cin >> a;
                if (dir.find(a) != dir.end()) {
	                x = j;
	                y = i;
	                d = dir[a];
                }
                grid[j][i] = cell[a];
            }
        }
        
        int stickers = 0;
        for (int i = 0; i < s; i++) {
            char c; cin >> c;
            int p = instr[c];
            
            d += p;
            if (d > 3) d = 0;
            if (d < 0) d = 3;
            if (p == instr['D'] || p == instr['E']) {
                continue;
            }
            else {
                int dx = (d == dir['L']) - (d == dir['O']);
                int dy = (d == dir['S']) - (d == dir['N']);
                
                if (0 > x + dx || x + dx >= m ||
                    0 > y + dy || y + dy >= n ||
                    grid[x + dx][y + dy] == cell['#'])
                    continue;
                
                grid[x][y] = '.';
                x += dx; y += dy;
                stickers += grid[x][y] == cell['*'];
            }
        }
        
        cout << stickers << "\n";
    }

    return 0;
}
