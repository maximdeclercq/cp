#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair
#define F first
#define S second
#define P pair
#define V vector

int p[1000000];

void make_set(int x) {
	p[x] = x;
}

int find_set(int x) {
	if (p[x] != x)
		p[x] = find_set(p[x]);
	return p[x];
}

void merge_sets(int x, int y) {
	int xs = find_set(x);
	int ys = find_set(y);
	p[ys] = xs;
}

int main() {
    ios_base::sync_with_stdio(false);
	cin.tie(0);
	
	while (true) {
		int n, m; cin >> n >> m;
		if (n == 0 && m == 0) break;
		
		V<P<int, P<int, int>>> edges;
		for (int i = 0; i < m; i++) {
			int u, v, w; cin >> u >> v >> w;
			edges.PB(MP(w, MP(u, v)));		
		}
		sort(edges.begin(), edges.end());
		
		for (int i = 0; i < n; i++) {
			make_set(i);
		}	
		
		int cnt = 0;
		int res = 0;
		for (int i = 0; i < m; i++) {
			int c = edges[i].F;
			P<int, int> e = edges[i].S;
			if (find_set(e.F) != find_set(e.S)) {
				merge_sets(e.F, e.S);
				cnt++;
				res = max(res, c);
			}
		}
		
		if (cnt == n - 1)
			cout << res << "\n";
		else
			cout << "IMPOSSIBLE\n";				
	}
		
    return 0;
}
