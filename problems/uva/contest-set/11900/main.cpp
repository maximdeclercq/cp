#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    
    int t = 0;
    cin >> t;

    for (int i = 1; i <= t; i++) {
        int n = 0; // number of count
        int p = 0; // max count
        int q = 0; // max weight
        cin >> n >> p >> q;

        int weights[n];

        for (int j = 0; j < n; j++) {
            int a = 0;
            cin >> a;

            weights[j] = a;
        }

        int count = 0;
        int weight = 0;
        for (int j = 0; j < n; j++) {
            weight += weights[j];
            if (count >= p || weight > q) {
                break;
            }
            count++;
        }
        cout << "Case " << i << ": " << count << endl;
    }
    return 0;
}