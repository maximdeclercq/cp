#include <bits/stdc++.h>
using namespace std;

#define PB push_back

bitset<100> graph[100];
bitset<100> visited;
bitset<100> initialVisited;

void initialDfs(int a) {
    if (initialVisited[a]) return;
    initialVisited[a] = true;
    
    for (int i = 0; i < 100; i++)
        if (graph[a][i]) initialDfs(i);
}

void dfs(int a, int b = -1) {
    if (visited[a] || a == b) return;
    visited[a] = true;
    
    for (int i = 0; i < 100; i++)
        if (graph[a][i]) dfs(i, b);
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int t; cin >> t;
    for (int i = 1; i <= t; i++) {
        int n; cin >> n;
        
        for (int j = 0; j < n; j++) graph[j].reset();
        
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < n; k++) {
                bool a; cin >> a;
                graph[j][k] = a;
            }
        }
        
        initialVisited.reset();
        initialDfs(0);
        
        
        cout << "Case " << i << ":";
        string o = "\n+"; for (int j = 0; j < 2 * n - 1; j++) o += "-"; o += "+\n"; 
        
        cout << o;
        for (int j = 0; j < n; j++) {
            visited.reset();
            dfs(0, j);
            cout << "|";
            for (int k = 0; k < n; k++)
                cout << (initialVisited[k] && !visited[k] ? "Y" : "N") << "|";
            cout << o;
        }
    }
       
    return 0;
}
