#include <bits/stdc++.h>
using namespace std;

int n;
char grid[105][105];

void eraseShip(int x, int y) {
    if (0 > x || x >= n || 0 > y || y >= n || grid[x][y] == '.') return;
    
    grid[x][y] = '.';
    eraseShip(x - 1, y);
    eraseShip(x + 1, y);
    eraseShip(x, y - 1);
    eraseShip(x, y + 1);
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int t; cin >> t;
    for (int i = 0; i < t; i++) {
        cin >> n;
        memset(grid, false, sizeof(grid));
        int count = 0;
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < n; k++) {
                char a; cin >> a;
                grid[k][j] = a;
            }
        }
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < n; k++) {
                if (grid[k][j] == 'x') {
                    eraseShip(k, j);
                    count++;
                }
            }
        }
        cout << "Case " << i + 1 << ": " << count << "\n";
    }

    return 0;
}
