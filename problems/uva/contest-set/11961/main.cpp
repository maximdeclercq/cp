#include <bits/stdc++.h>
using namespace std;

char dna[4] = {'A', 'C', 'G', 'T'};

set<string> mutations;

void mutate(string original, int index) {
    string mutation(original);
    for (int l = 0; l < 4; l++) {
        mutation[index] = dna[l];
        mutations.insert(mutation);
    }
}

void mutateAll() {
    set<string> oldMutations = mutations;
    for (auto mutation : oldMutations) {
        for (int k = 0; k < (int)mutation.size(); k++) {
            mutate(mutation, k);
        }
    }
}

void printMutations() {
    cout << mutations.size() << "\n";
    for (auto mutation : mutations) {
        cout << mutation << "\n";
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        int n, k;
        string s;
        cin >> n >> k >> s;

        mutations.clear();
        mutations.insert(s);

        for (int j = 0; j < k; j++) {
            mutateAll();
        }

        printMutations();
    }

    return 0;
}