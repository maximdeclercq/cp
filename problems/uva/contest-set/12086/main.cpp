#include <bits/stdc++.h>
using namespace std;

struct Node {
    Node * l;
    Node * r;

    int i, j; // interval [i, j]
    int val; // sum
};

vector<int> res;

/* Build tree on [i, j] */
Node * build(int i, int j) {
    if (i == j) {
        return new Node { nullptr, nullptr, i, j, res[i] };
    }
    else {
        Node * l = build(i, (i + j) / 2);
        Node * r = build((i + j) / 2 + 1, j);
        return new Node { l, r, i, j, l->val + r->val };
    }
}

int measure(Node * cur, int i, int j) {
    if (cur->j < i || cur->i > j) {
        return 0;
    }
    else if (cur->i >= i && cur->j <= j) {
        return cur->val;
    }
    else {
        int lVal = measure(cur->l, i, j);
        int rVal = measure(cur->r, i, j);
        return lVal + rVal;
    }
}

void setVal(Node * cur, int i, int newVal) {
    if (cur->l == nullptr) {
        cur->val = newVal;
    }
    else {
        if (i <= cur->l->j) { //left
            setVal(cur->l, i, newVal);
        }
        else { //right
            setVal(cur->r, i, newVal);
        }
        // update
        cur->val = cur->l->val + cur->r->val;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int c = 0;
    while (1) {
        int n;
        cin >> n;

        if (n == 0) break;

        ++c;
        cout << ((c == 1) ? "" : "\n") << "Case " << c << ":\n";

        res.reserve(n);

        for (int i = 0; i < n; i++) {
            cin >> res[i];
        }

        Node * root = build(0, n - 1);

        while (1) {
            string action;
            cin >> action;

            if (action == "S") {
                int x, r;
                cin >> x >> r;
                --x;
                setVal(root, x, r);
            }
            else if (action == "M") {
                int x, y;
                cin >> x >> y;
                --x; --y;
                cout << measure(root, x, y) << "\n";
            }
            else {
                break;
            }
        }
    }
}
