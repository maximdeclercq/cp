 #include <bits/stdc++.h>
using namespace std;

int child[50005];
int dist[50005];
bitset<50005> visited;

int dfs(int a) {
    if (visited[a]) return 0;
    visited[a] = true;
    
    dist[a] = dfs(child[a]) + 1;
    visited[a] = false;
    
    return dist[a];
}


int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);

    int t; cin >> t;
    for (int i = 1; i <= t; i++) {
        int n; cin >> n;

        memset(child, 0, sizeof(int) * (n + 1));
        memset(dist, 0, sizeof(int) * (n + 1));

        for (int j = 1; j <= n; j++) {
            int a, b; cin >> a >> b;
            child[a] = b;
        }

        int m = 0;
        int mi = 1;
        visited.reset();
        for (int j = 1; j <= n; j++) {
            if (!dist[j]) dfs(j);
            if (dist[j] > m)
                m = dist[j], mi = j;
        }

        cout << "Case " << i << ": " << mi << "\n";
    }

    return 0;
}
