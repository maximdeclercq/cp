#include <bits/stdc++.h>
using namespace std;

#define LSO(i) ((i) & (-i)) 

vector<int> ft;

int query(int i) {
    int prod = 1;
    for (; i > 0; i -= LSO(i))
        prod *= ft[i];
    return prod;
}

int n = 0;
void update(int i, int v) {
    for (; i <= n; i += LSO(i))
        ft[i] *= v;
}


int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    while (cin >> n) {
        ft.resize(n);

        int k;
        cin >> k;

        for (int i = 1; i <= n; i++) {
            int v;
            cin >> v;
            update(i, v);
        }
        for (int i = 0; i < k; i++) {
            char c;
            int a, b;
            cin >> c >> a >> b;
            if (c == 'C') {
                update(a, b);
            }
            else if (c == 'P') {
                int qa = query(a - 1);
                int qb = query(b);
                int p = qa != 0 ? qb / qa : 0;
                if (p > 0) {
                    cout << '+';
                }
                else if (p < 0) {
                    cout << '-';
                }
                else {
                    cout << '0';
                }
            }
        }
        cout << "\n";
    }
}
