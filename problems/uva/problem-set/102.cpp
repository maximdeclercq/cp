#include <iostream>
#include <string>
#include <algorithm>
#include <limits>
using namespace std;

static const int perms[6][3]= {{0, 1, 2}, {0, 2, 1}, {1, 0, 2}, {1, 2, 0}, {2, 0, 1}, {2, 1, 0}};
static const string formats[6] = {"BCG","BGC","CBG","CGB","GBC","GCB"};

int main() {
    ios_base::sync_with_stdio(false);
    
    int bins[3][3]; // BGC -> BCG
    while (cin >> bins[0][0]) {

        cin >> bins[0][2] >> bins[0][1]
            >> bins[1][0] >> bins[1][2] >> bins[1][1]
            >> bins[2][0] >> bins[2][2] >> bins[2][1];

        int sum = 0;
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                sum += bins[i][j];
            }
        }

        int index = 0;
        int maxMoves = INT32_MAX;
        for (int i = 0; i < 6; ++i) {
            int moves = sum;
            for (int j = 0; j < 3; ++j) {
                moves -= bins[j][perms[i][j]];
            }
            if (moves < maxMoves) {
                index = i;
                maxMoves = moves;
            }
        }

        cout << formats[index] << " " << maxMoves << endl;
    }

    return 0;
}