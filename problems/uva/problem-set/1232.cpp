#include <bits/stdc++.h>
using namespace std;

vector<int> overlap;
vector<int> height;
vector<int> lazy;

void propagate(int p, int i, int j) {
    if (lazy[p] != 0) {
        if (lazy[p] >= height[p]) {
            overlap[p] += j - i + 1;
            height[p] = lazy[p];
        }

        if (i != j) {
            lazy[2 * p] = lazy[p];
            lazy[2 * p + 1] = lazy[p];
        }

        lazy[p] = 0;
    }
}

int query(int p, int i, int j, int a, int b) {
    propagate(p, i, j);

    if (j < a || i > b) {
        return 0;
    }
    else if (i >= a && j <= b) {
        return overlap[p];
    }
    else {
        int m = (i + j) / 2;
        return query(2 * p, i, m, a, b)
             + query(2 * p + 1, m + 1, j, a, b);
    }
}

void update(int p, int i, int j, int a, int b, int newHeight) {
    propagate(p, i, j);

    if (j < a || i > b) {
        return;
    }
    else if (i >= a && j <= b) {
        lazy[p] = newHeight;
        propagate(p, i, j);
    }
    else {
        int m = (i + j) / 2;
        update(2 * p, i, m, a, b, newHeight);
        update(2 * p + 1, m + 1, j, a, b, newHeight);

        overlap[p] = overlap[2 * p] + overlap[2 * p + 1];
        height[p] = max(max(height[2 * p], height[2 * p + 1]), height[p]);
    }
}

void printTree(int p, int i, int j, int indent = 0) {
    propagate(p, i, j);

    string a;
    for (int i = 0; i < indent; i++) a.append("    ");
    cout << a << "[" << i << "-" << j << "]" << " = " << overlap[p] << " (" << height[p] << ")\n";
    if (i != j) {
        int m = (i + j) / 2;
        printTree(2 * p, i, m, indent + 1);
        printTree(2 * p + 1, m + 1, j, indent + 1);
    }
}

//const int MAX_N = 100010;
const int MAX_N = 16;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    int c;
    cin >> c;

    for (int i = 0; i < c; i++) {
        int n;
        cin >> n;

        overlap.clear();
        overlap.assign(2 * MAX_N, 0);
        height.clear();
        height.assign(2 * MAX_N, 0);
        lazy.clear();
        lazy.assign(2 * MAX_N, 0);

        for (int  j = 0; j < n ; j++) {
            int l, r, h;
            cin >> l >> r >> h;

            update(1, 1, MAX_N, l, r - 1, h);
        }

        printTree(1, 1, MAX_N);

        cout << overlap[1] << "\n";
    }

    int null;
    cin >> null;
}
