#include <iostream>
#include <string>
using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    
    int t = 0;
    cin >> t;

    for (int i = 0; i < t; i++) {
        string s = "";
        cin >> s;

        int score = 0;
        int counter = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s[i] == 'O') {
                counter++;
                score += counter;
            }
            else {
                counter = 0;
            }
        }

        cout << score << endl;
    }
    return 0;
}