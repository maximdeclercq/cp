#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair

vector<int> graph[26];
vector<string> data;
vector<int> toposort;
bitset<26> valid;
bitset<26> visited;

void dfs(int a) {
    if (visited[a]) return;
    visited[a] = true;
    for (int b : graph[a])
        dfs(b);
    toposort.PB(a);
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    
    string s;
    while (cin >> s) {
        if (s != "#") {
            data.PB(s);
        }
        else {
            if (data.size() == 1) {
                cout << data[0][0] << "\n";
            }
            else {
                for (int i = 0; i < (int)data.size() - 1; i++) {
                    char a, b;
                    for (int j = 0; (a = data[i][j]) == (b = data[i + 1][j]); j++);
                    
                    int ai = a - 'A', bi = b - 'A';
                    if (ai >= 0)
                        graph[ai].PB(bi), valid[ai] = true;
                    valid[bi] = true;
                }
                
                for (int i = 0; i < 26; i++)
                    if (valid[i]) dfs(i);
                
                for (int i = toposort.size() - 1; i >= 0; i--)
                    cout << (char)(toposort[i] + 'A');
                cout << "\n";
            }
            
            for (int i = 0; i < 26; i++) graph[i].clear();
            data.clear();
            toposort.clear();
            valid.reset();
            visited.reset();
        }
    }
    
    return 0;
}
