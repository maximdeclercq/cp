#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second
#define BS bitset

int t;
V<int> adj[21];
BS<21> visited;

int num = 0;
int ind = -1;
int res[21];
void dfs(int i = 0) {
	ind++;
	res[ind] = i;
	if (i == t) {
		num++;
		for (int i = 0; i < ind; i++) {
			cout << res[i] + 1 << " ";
		}
		cout << t + 1 << "\n";
	}
		
	visited[i] = true;
	sort(adj[i].begin(), adj[i].end());
	for (int j : adj[i]) {
		if (!visited[j]) {
			dfs(j);
		}
	}
	visited[i] = false;
	ind--;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    int c = 0;
	while (cin >> t) {
		t--;
		num = 0;
		ind = -1;
		for (int i = 0; i < 21; i++)
			adj[i].clear();
		visited.reset();
		
		while (true) {
			int a, b; cin >> a >> b;
			if (a == 0 && b == 0) break;
			adj[--a].PB(--b);
			adj[b].PB(a);
		}
		
		cout << "CASE " << ++c << ":\n";
		dfs();
		cout << "There are " << num << " routes from the firestation to streetcorner " << t + 1 << ".\n";
	}    
    
    return 0;
}
