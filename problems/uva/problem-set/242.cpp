#include <bits/stdc++.h>
using namespace std;

map<int, int> val;
map<int, bool> ready;
vector<int> stamps[10];

const int INF = 1000000;

int count(int n, int k) { 
    if (n < 0) return INF;
    if (n == 0) return 0;
    if (ready[n]) return val[n];
    
    int best = INF;

    for (int s : stamps[k]) {
        //cout << n << " " << s << " " << count(n - s, k) << "\n";
        best = min(best, count(n - s, k) + 1);
    }
 
    val[n] = best;
    ready[n] = true;
    return best;
}

int maxCoverage(int max, int k) {
    int i = 1;
    for (; count(i, k) <= max; i++);
    return i;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);

    while (1) {
        int s, n;        
        cin >> s;

        if (s <= 0) break;

        cin >> n;

        for (int i = 0; i < n; i++) {            
            int a;
            cin >> a;
            stamps[i].clear();
            for (int j = 0; j < a; j++) {
                int b;
                cin >> b;
                stamps[i].push_back(b);
            }
        }

        int max = 0;
        int maxI = -1;
        for (int i = 0; i < n; i++) {
            val.clear();
            ready.clear();
            int a = maxCoverage(s, i) - 1;
            if (a > max) {
                max = a;
                maxI = i;
            }
            else if (a == max) {
                if (stamps[maxI].size() > stamps[i].size()) {
                    max = a;
                    maxI = i;
                }
                else if (stamps[maxI].size() == stamps[i].size()) {
                    if (stamps[maxI][stamps[maxI].size() - 1] > stamps[i][stamps[i].size() - 1]) {
                        max = a;
                        maxI = i;
                    }
                    else if (stamps[maxI][stamps[maxI].size() - 1] == stamps[i][stamps[i].size() - 1]) {
                        if (stamps[maxI][stamps[maxI].size() - 2] > stamps[i][stamps[i].size() - 2]) {
                            max = a;
                            maxI = i;
                        }
                    }
                }
            }
        }

        cout << "max coverage =" << setw(4) << max << setw(0) << " :";
        for (int s : stamps[maxI]) {
            cout << setw(3) << s;
        }
        cout << "\n";
    }

    return 0;
}