#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair

bitset<25> visited(0);
void dfs(int c, vector<vector<int>> & neigh, vector<int> & st) {
    if (visited[c]) return;
    visited[c] = true;
    
    for (int a : neigh[c])
        dfs(a, neigh, st);
        
    st.PB(c);
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    
    int c = 0;
    while (true) {
        int n, m; cin >> n >> m;
        if (n== 0 && m == 0) break;
        
        map<string, int> stoi;
        vector<string> itos(n);
        
        vector<vector<int>> neigh(n, vector<int>());
        vector<vector<int>> neighT(n, vector<int>());
        
        int num = 0;
        for (int i = 0; i < m; i++) {
            string a, b; cin >> a >> b;
            
            if (stoi.find(a) == stoi.end())
                stoi[a] = num, itos[num] = a, num++;
            if (stoi.find(b) == stoi.end())
                stoi[b] = num, itos[num] = b, num++;
            
            int ai = stoi[a], bi = stoi[b];
            
            neigh[ai].PB(bi);
            neighT[bi].PB(ai);
        }
        
        vector<int> toposort;
        
        visited.reset();
        for (int i = 0; i < n; i++)
            dfs(i, neigh, toposort);
        
        cout << (c > 0 ? "\n" : "");
        cout << "Calling circles for data set " << ++c << ":\n";
        
        visited.reset();
        for (int i = n - 1; i >= 0; i--) {
            if (!visited[toposort[i]]) {
		        vector<int> scc;
		        dfs(toposort[i], neighT, scc);
		        
                for (int j = 0; j < (int)scc.size(); j++)
	                cout << (j > 0 ? ", " : "") << itos[scc[j]];
	            cout << "\n";
            }
        }
    }
    
    
    return 0;
}
