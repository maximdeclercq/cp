
#include <bits/stdc++.h>
using namespace std;

#define PB push_back

vector<int> graph[105];
int num[105];
int low[105];
bitset<105> visited;
bitset<105> ap;
int counter;
int rChildren;

void dfs(int a, int p = -1) {
	num[a] = low[a] = counter++;
	
	visited[a] = true;
	for (int b : graph[a]) {
		if (!visited[b]) {
			dfs(b, a);
			if (p < 0) rChildren++;
			else if (low[b] >= num[a]) ap[a] = true;
			low[a] = min(low[a], low[b]);
		}
		else if (b != p) {
			low[a] = min(low[a], num[b]);
		}
	}
}


void reset() {
	for (int i = 0; i < 105; i++) graph[i].clear();
	memset(num, 0, sizeof(num));
	memset(low, 0, sizeof(low));
	visited.reset();
	ap.reset();
	rChildren = counter = 0;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    while (true) {
		reset();
		
		int n; cin >> n;
		if (n == 0) break;
		while (true) {
			string s; getline(cin, s); istringstream is(s);
			int a = -1, b = -1; is >> a;
			if (a == 0) break;
			while (is >> b) {
				graph[a].PB(b);
				graph[b].PB(a);
			}
		}
		
		for (int i = 1; i <= n; i++) {
			if (!visited[i]) {
				rChildren = 0;
				dfs(i);
				if (rChildren > 1) ap[i] = true;
			}
		}
		
		cout << ap.count() << "\n";
	}

    return 0;
}
