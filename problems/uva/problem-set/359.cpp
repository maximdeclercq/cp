#include <bits/stdc++.h>
using namespace std;

#define V vector
#define PB push_back
#define P pair
#define MP make_pair
#define F first
#define S second

int n, m;
int in_degree[1000000];
int out_degree[1000000];
int sex[1000000];

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    int g = 0;
    while (cin >> n) {
    	cin >> m;
    
    	memset(in_degree, 0, sizeof(in_degree));
    	memset(out_degree, 0, sizeof(out_degree));
    	memset(sex, 0, sizeof(sex));
    
    	for (int i = 0; i < m; i++) {
    		int x, y; cin >> x >> y;
    		if (sex[--x] != 0) {
				in_degree[--y]++;
				out_degree[x] += sex[x];
    		}
    		else {
    			sex[x] = -in_degree[y]++;
				out_degree[x] += sex[x];
    		}
    	}
    	
    	int i;
    	int in_zero = 0;
    	int out_zero = 0; 
    	for (int i = 0; i < n; i++) {
    		cout << in_degree[i] << " " << out_degree[i] << "\n";
    	}   	
    	for (i = 0; i < n; i++) {
    		if (in_degree[i] == 0) in_zero++;
    		if (out_degree[i] == 0) out_zero++;
    		if (in_degree[i] == 0 && out_degree[i] == 0) continue;
    		if (out_degree[i] == 0 && in_degree[i] != 2) break;
    	}
    	if (i == n && in_zero > 0 && out_zero > 0) {
    		cout << "Graph number " << ++g << " is sexy\n";
    	}
    	else {
    		cout << "Graph number " << ++g << " is not sexy\n";
    	}
    }
    
    return 0;
}
