#include <bits/stdc++.h>
using namespace std;

int m, s;
int coins[101];

int memo[101][101 * 501];

int solve(int i = 0, int v = 0) {
	if (2 * v > s) return INT_MIN;
	if (i >= m) return 0;
	
	if (memo[i][v] >= 0) return memo[i][v];

	return memo[i][v] = max(solve(i + 1, v), coins[i] + solve(i + 1, coins[i] + v));
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    
    int n; cin >> n;
    while (n--) {
    	cin >> m;
    	
    	s = 0;
    	for (int i = 0; i < m; i++) {
    		cin >> coins[i];
    		s += coins[i];
    	}
    	sort(coins, coins + m);//, greater<int>());
    	
    	for (int i = 0; i < m; i++) fill(memo[i], memo[i] + 501 * m, -1);
    	cout << s - 2 * solve() << "\n";
    }

    return 0;
}
