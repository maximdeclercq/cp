#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    
    while (1) {
        int h = 0; // height of the well
        float u = 0; // climb dist
        int d = 0; // slide dist
        int f = 0; // f fatigue
        cin >> h >> u >> d >> f;

        if (h == 0) break;

        float pos = 0;
        int day = 0;
        float fatigue  = u * f / 100.f;
        while (1) {
            //day
            day++;
            pos += max(0.f, u);
            if (pos > h) {
                cout << "success on day " << day << endl; 
                break;
            }

            //night
            pos -= d;
            if (pos < 0) {
                cout << "failure on day " << day << endl; 
                break;
            }

            u -= fatigue;
        }
    }

    return 0;
}