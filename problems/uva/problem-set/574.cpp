#include <bits/stdc++.h>
using namespace std;

int t, n;
vector<int> x(12);

bool solve(int s = 0, int c = 0, string o = "") {
    if (s > t) {
        return false;
    }
    bool b = false;
    int l = -1;
    for (int i = c; i < n; i++) {
        if (l != x[i]) {
            if (t == s + x[i]) {
                cout << o << x[i] << "\n";
                b |= true;
            }
            b |= solve(s + x[i], i + 1, o + to_string(x[i]) + "+");
        }
        l = x[i];
    }
    return b;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    while (true) {
        cin >> t >> n;
        if (t == 0 && n == 0) break;
        
        for(int i = 0; i < n; i++) cin >> x[i];
        
        cout << "Sums of " << t << ":\n";
        cout << (solve() ? "" : "NONE\n");
    }

    return 0;
}
