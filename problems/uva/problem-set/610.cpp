#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair

#define N 1000

enum state {
	UNVISITED,
	OPENED,
	CLOSED
};

int n, t;

vector<int> graph[N];
state st[N]; 

int num[N];
int low[N];

vector<pair<int, int>> dirs;

void dfs(int a, int p = -1) {
	num[a] = low[a] = t++;
	st[a] = OPENED; 
	for (int b : graph[a]) {
		if (st[b] != CLOSED && b != p) dirs.PB(MP(a, b));

		if (st[b] == UNVISITED) {
			dfs(b, a);
			if (low[b] > num[a]) dirs.PB(MP(b, a));
			low[a] = min(low[a], low[b]);
		}
		else if (b != p) {
			low[a] = min(low[a], num[b]);
		}
	}
	st[a] = CLOSED; 
}

void reset() {
	n = t = 0;

	for (int i = 0; i < N; i++) graph[i].clear();
	memset(st, UNVISITED, sizeof(st));

	memset(num, 0, sizeof(num));
	memset(low, 0, sizeof(low));

	dirs.clear();
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

	int c = 0;
	while (true) {
		reset();

		int n, m; cin >> n >> m;
		if (n == 0 && m == 0) break;

		for (int i = 0; i < m; i++) {
			int x, y; cin >> x >> y;
			graph[--x].PB(--y);
			graph[y].PB(x);
		}
		
		cout << ++c << "\n\n";

		for (int i = 0; i < n; i++)
			if (st[i] == UNVISITED) dfs(i);

		for (auto d: dirs)
			cout << ++d.first << " " << ++d.second << "\n";
		cout << "#\n";
	}
    
    return 0;
}
