#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair

#define MAXn 25
#define MAXN 25000

int N, n;
int l[MAXn];
int memo[MAXn][MAXN];

int solve(int i = 0, int w = 0) {
	if (w > N) return INT_MIN;
	if (i >= n) return 0;

	if (memo[i][w] >= 0) return memo[i][w];
	return memo[i][w] = max(solve(i + 1), l[i] + solve(i + 1, w + l[i]));
}



int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    
    while (cin >> N) {
		cin >> n;
		
		memset(l, 0, sizeof(l));
		memset(memo, -1, sizeof(memo));
		
    	for (int i = 0; i < n; i++) cin >> l[i];
    	sort(l, l + n, greater<int>());
    	
		vector<int> res;
		int sum = solve();
		
    	for (int a : res) cout << a << " ";
    	cout << "sum:" << sum << "\n";
    }
    
    return 0;
}
