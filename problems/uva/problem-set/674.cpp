#include <bits/stdc++.h>
using namespace std;

int coins[5] = { 50, 25, 10, 5, 1 };
int memo[5][7489];

int count(int a, int b = 0) {
	if (a < 0) return 0;
	if (a == 0) return 1;
	if (memo[b][a] > 0) return memo[b][a];
	int c = 0;
	for (int i = b; i < 5; i++) {
		c += count(a - coins[i], i);
	}
	return memo[b][a] = c;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    memset(memo, 0, sizeof(memo));
    
    int a;
    while(cin >> a) {
        cout << count(a) << "\n";
    }

    return 0;
}
