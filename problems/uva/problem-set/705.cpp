
#include <bits/stdc++.h>
using namespace std;

#define A(a) ((a) - 1 + 2 * ((a) % 2))
#define B(a) ((a) - (a) % 2)
#define P(a, b) ((a) + 150 * (b))
#define INB(x, a, b) ((a) <= (x) && (x) <= (b))
#define OUTB(x, a, b) ((x) < (a) || (b) < (x))
#define MAX4(a, b, c, d) (max(max((a), (b)), max((c), (d))))

int w, h, xs, ys;
bool graph[75][75]; // true = '/', false = '\'
bitset<150 * 150> visited;

int dfs(int x, int y, int depth = 0) {
    if (x == xs && y == ys && depth > 2) return depth;
    if (depth == 0) xs = x, ys = y;
    
    if (OUTB(x, 0, 2 * w - 1) || OUTB(y, 0, 2 * h - 1)) return 0;
    if (x % 2 ^ y % 2 ^ !graph[x / 2][y / 2]) return 0;
        
    if (visited[P(x, y)]) return 0;
    visited[P(x, y)] = true;
    
    return MAX4(dfs(A(x), B(y), depth + 1),
                dfs(A(x), B(y) + 1, depth + 1),
                dfs(B(x), A(y), depth + 1),
                dfs(B(x) + 1, A(y), depth + 1)
    );
}

void reset() {
    xs = -1; ys = -1;
    memset(graph, false, sizeof(graph));
    visited.reset();
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);

    int maze = 0;
    while (true) {
        reset();

        cin >> w >> h;
        if (w == 0 && h == 0) break;

        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                char a; cin >> a;
                graph[x][y] = a == '/';
            }
        }

        int m = 0;
        int c = 0;
        for (int y = 0; y < 2 * h; y++) {
            for (int x = 0; x < 2 * w; x++) {
                int k = dfs(x, y);
                if (k > 1) c++;
                if (k > m) m = k;
            }
        }

        cout << "Maze #" << ++maze << ":\n";
        if (c == 0)
            cout << "There are no cycles.\n\n";
        else
            cout << c << " Cycles; the longest has length " << m << ".\n\n";
    }

    return 0;
}
