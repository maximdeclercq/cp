#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair
#define N 20

int n;
bool path;
bool graph[N][N];
int ans[N];
bitset<N> visited;

map<char, int> ctoi;
vector<char> itoc;

void dfs(int p = 0) {
    if (p == n) {
        for (int i = 0; i < n; i++)
            cout << (i > 0 ? " " : "") << itoc[ans[i]];
        cout << "\n";
        path = true;
    }

    for (int i = 0; i < n; i++) {
        if (!visited[i]) {
            int j = 0;
            // check for conflict with constraints
            for (; j < p; j++)
                if (graph[i][ans[j]]) break;
            // no conflict found
            if (j == p) {
                visited[i] = true;
                ans[p] = i, dfs(p + 1);
                visited[i] = false;
            }
        }
    }
}

void reset() {
    n = 0; path = false;
    memset(graph, false, sizeof(graph));
    memset(ans, 0, sizeof(ans));
    visited.reset();
    ctoi.clear();
    itoc.clear();
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    
    int t; cin >> t;
    cin.ignore(256, '\n');
    while (t--) {
        reset();
        
        cin.ignore(256, '\n');
        
        string a; getline(cin, a); sort(a.begin(), a.end()); int as = a.size();
        for (int i = 0; i < as; i++) {
	        if (a[i] < 'A' || 'Z' < a[i]) continue;
	        ctoi[a[i]] = n; n++;
	        itoc.PB(a[i]);
	    }
            
        string b; getline(cin, b); int bs = b.size();
        for (int i = 0; i < bs; i += 4) {
            int x = ctoi[b[i]];
            int y = ctoi[b[i + 2]];
            graph[x][y] = true;
        }
        
        dfs();
            
        if (!path) cout << "NO\n";
        cout << (t > 0 ? "\n" : "");
    }
    
    return 0;
}
