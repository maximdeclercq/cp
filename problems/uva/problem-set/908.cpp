#include <bits/stdc++.h>
using namespace std;

#define PB push_back
#define MP make_pair
#define F first
#define S second

vector<pair<uint64_t, pair<uint64_t, uint64_t>>> edges;
uint64_t p[1000020];

void make_set(uint64_t x) {
	p[x] = x;
}

uint64_t find_set(uint64_t x) {
	if (p[x] != x)
		p[x] = find_set(p[x]);
	return p[x];
}

void merge_sets(uint64_t x, uint64_t y) {
	uint64_t xs = find_set(x);
	uint64_t ys = find_set(y);
	p[ys] = xs;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    
    uint64_t n;
    uint64_t t = 0;
    while (cin >> n) {
    	if (t++ != 0)
    		cout << "\n";
    
    	edges.clear();
    	uint64_t orig = 0;
		for (uint64_t i = 0; i < n - 1; i++) {
			uint64_t a, b, c; cin >> a >> b >> c; a--; b--;
    		edges.PB(MP(c, MP(a, b)));
    		orig += c;
		}
		cout << orig << "\n";
		
		uint64_t k; cin >> k;
		for (uint64_t i = 0; i < k; i++) {
			uint64_t a, b, c; cin >> a >> b >> c; a--; b--;
    		edges.PB(MP(c, MP(a, b)));
		}
		
		uint64_t m; cin >> m;
		for (uint64_t i = 0; i < m; i++) {
			uint64_t a, b, c; cin >> a >> b >> c;
		}
		
		for (uint64_t i = 0; i < n + 2 * k; i++) {
			make_set(i);
		}
		
		uint64_t newv = 0;
		sort(edges.begin(), edges.end());
		for (uint64_t i = 0; i < edges.size(); i++) {
			pair<uint64_t, uint64_t> e = edges[i].S;
			if (find_set(e.F) != find_set(e.S)) {
				newv += edges[i].F;
				merge_sets(e.F, e.S);
			}
		}
		
		cout << newv << "\n";
    }
    
    return 0;
}
